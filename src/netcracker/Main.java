package netcracker;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Дмитрий
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws CloneNotSupportedException {
/*
        try {
/*
            /////////////Flat/////////////////////////////
            Flat f1 = new Flat();
            Flat f2 = new Flat(45.2);
            Flat f3 = new Flat(0, 23);
            f1.setNumberOfRooms(6);
            f1.setSquare(-45.2);
            System.out.println(f1.getNumberOfRooms() + " " + f1.getSquare());
            System.out.println(f2.getNumberOfRooms() + " " + f2.getSquare());
            System.out.println(f3.getNumberOfRooms() + " " + f3.getSquare());
*/
        //////////////DwellingFloor////////////////////
/*
            Flat[] array = {f1, f2, f3};
            DwellingFloor df1 = new DwellingFloor(4);
            DwellingFloor df2 = new DwellingFloor(array);
            System.out.println(df1.getBestSpace().getSquare() + " " + df2.getBestSpace().getSquare());
            System.out.println(df1.getSpace(3).getNumberOfRooms() + " " + df2.getSpace(1).getSquare());
            System.out.println(df1.getSpace(10).getNumberOfRooms() + " " + df1.getSpace(10).getSquare());//проверка неправильного ввода
            System.out.println(df1.numberOfSpaces() + " " + df2.numberOfSpaces());
            System.out.println(df1.totalRooms() + " " + df2.totalSquare());
            System.out.println(df1.totalSquare() + " " + df2.totalRooms());
            df1.addSpace(3, f3);
            df2.addSpace(10, f1);//проверка неправильного ввода
            System.out.println(df1.getSpace(3).getSquare() + " " + df2.totalSquare() + " " + df2.numberOfSpaces());
            df1.changeSpace(0, f2);
            df2.changeSpace(6, f1);
            System.out.println(df1.getSpace(0).getSquare() + " " + df2.totalSquare());
            df1.changeSpace(10, f3);//не сработало
            System.out.println(df1.numberOfSpaces());
            df1.deleteSpace(0);
            df1.deleteSpace(0);
            df1.deleteSpace(0);
            df1.deleteSpace(0);
            df1.deleteSpace(0);
            df1.deleteSpace(0);
            df2.deleteSpace(10);
            System.out.println(df1.getSpace(0).getSquare() + " " + df2.totalSquare() + " " + df1.numberOfSpaces() + " " + df2.numberOfSpaces());
*/
/*
            //0 этаж
            Flat f1 = new Flat(101.0, 4);
            Flat f2 = new Flat();
            //1 этаж
            Flat f3 = new Flat(36.5, 2);
            Flat f4 = new Flat(39.0, 0);
            Flat f5 = new Flat(3, 2);
            Flat f6 = new Flat(97.34, 3);
            Flat f7 = new Flat(18.7, 0);
            //2 этаж
            Flat f8 = new Flat(10.4, 3);
            Flat f9 = new Flat(162.8, 4);
            Flat f10 = new Flat(28.14, 4);
            Flat f11 = new Flat(1, 2);
            //3 этаж
            Flat f12 = new Flat(58, 0);
            //4 этаж
            Flat f13 = new Flat(5, 6);
            Flat f14 = new Flat(7, 3);
            /////тесты для Flat/////
            System.out.println(f1.getNumberOfRooms() + " " + f2.getSquare());
            f3.setNumberOfRooms(4);
            f4.setSquare(23.3);
            System.out.println(f3.getNumberOfRooms() + " " + f4.getSquare());
            System.out.println(f4.getNumberOfRooms() + " " + f3.getSquare());
            /////тесты для DwellingFloor////
            DwellingFloor df1 = new DwellingFloor(3);
            Flat[] array = {f8, f9, f10, f11};//2 этаж
            DwellingFloor df2 = new DwellingFloor(array);
            System.out.println(df1.getBestSpace().getSquare() + " " + df2.numberOfSpaces());
            System.out.println(df1.getSpace(0).getSquare());
            df2.addSpace(4, f14);
            System.out.println(df2.getSpace(0).getSquare() + " " + df2.getSpace(1).getSquare() + " " + df2.getSpace(2).getSquare() + " " + df2.getSpace(3).getSquare() + " " + df2.getSpace(4).getSquare());
            df1.changeSpace(1, f12);
            System.out.println(df1.getSpace(1).getSquare());
            df1.deleteSpace(0);
            System.out.println(df1.numberOfSpaces() + " " + df1.totalRooms() + " " + df1.totalSquare());
            System.out.println(df1.arrayOfSpaces().length);
            ////тесты для Dwelling/////
            int[] arr = {3, 3, 5, 4, 1};//1 здание(5этажное,по умолчанию)
            Dwelling d1 = new Dwelling(5, arr);//5этажное здание(здание 1)
            Flat[] floor0 = {f1, f2};//0 этаж
            Flat[] floor1 = {f3, f4, f5, f6, f7};//1 этаж
            Flat[] floor2 = {f8, f9, f10, f11};//2 этаж
            Flat[] floor3 = {f12};//3 этаж
            Flat[] floor4 = {f13, f14};//4 этаж
            DwellingFloor Floor0 = new DwellingFloor(floor0);//0 этаж
            DwellingFloor Floor1 = new DwellingFloor(floor1);//1 этаж
            DwellingFloor Floor2 = new DwellingFloor(floor2);//2 этаж
            DwellingFloor Floor3 = new DwellingFloor(floor3);//3 этаж
            DwellingFloor Floor4 = new DwellingFloor(floor4);//4 этаж
            DwellingFloor[] dfarray = {Floor0, Floor1, Floor2, Floor3, Floor4};//здание 2
            Dwelling d2 = new Dwelling(dfarray);//здание 2
            System.out.println(d2.getSpace(8).getSquare());
            Space[] flats1;
            flats1 = d2.getArrayOfSpaces();
            for (int i = 0; i < d2.getNumberOfSpaces(); i++) {
                System.out.print(flats1[i].getSquare() + " ");//сортированная последовательность квартир в доме
            }
            System.out.println("\n");
            System.out.println("1ое здание");
            ///вывод здания 1////
            for (Floor df : d1.getArrayOfFloors()) {
                for (Space f : df.arrayOfSpaces()) {
                    System.out.print(f.getNumberOfRooms() + " " + f.getSquare() + "  ");
                }
                System.out.println();
            }
            System.out.println("2ое здание");
            /////вывод здания 2////
            for (Floor df : d2.getArrayOfFloors()) {
                for (Space f : df.arrayOfSpaces()) {
                    System.out.print(f.getNumberOfRooms() + " " + f.getSquare() + "  ");
                }
                System.out.println();
            }
            System.out.println();
            ///////////////////////
            d1.addSpace(16, f3);
            Floor[] dwellingFloor;
            dwellingFloor = d1.getArrayOfFloors();
            for (Floor df : dwellingFloor) {
                Space[] flat;
                flat = df.arrayOfSpaces();
                for (Space f : flat) {
                    System.out.print(f.getNumberOfRooms() + " " + f.getSquare() + "  ");
                }
                System.out.println();
            }
            System.out.println();
            d1.deleteSpace(4);
            for (Floor df : d1.getArrayOfFloors()) {
                for (Space f : df.arrayOfSpaces()) {
                    System.out.print(f.getNumberOfRooms() + " " + f.getSquare() + "  ");
                }
                System.out.println();
            }
            System.out.println();
            d2.changeOfSpace(1, f1);
            for (Floor df : d2.getArrayOfFloors()) {
                for (Space f : df.arrayOfSpaces()) {
                    System.out.print(f.getNumberOfRooms() + " " + f.getSquare() + "  ");
                }
                System.out.println();
            }
            System.out.println();
            System.out.println(d2.getNumberOfSpaces() + " " + d2.getNumberOfFloors() + " " + d2.getNumberOfRooms() + " " + d2.getSquareOfRooms() + "\n");
            System.out.println(d1.getNumberOfSpaces() + " " + d1.getNumberOfFloors() + " " + d1.getNumberOfRooms() + " " + d1.getSquareOfRooms() + "\n");
*/
/*
            NodeSingly n0 = new NodeSingly();
            NodeSingly n1 = new NodeSingly();
            NodeSingly n2 = new NodeSingly();
            NodeSingly n3 = new NodeSingly();
            NodeSingly n4 = new NodeSingly();
            NodeSingly n5 = new NodeSingly();
            NodeSingly n6 = new NodeSingly();
            NodeSingly n7 = new NodeSingly();
            SinglyLinkedList l = new SinglyLinkedList();
            l.addNode(0, n0);
            l.addNode(1, n1);
            l.addNode(2, n2);
            l.addNode(3, n3);
            l.addNode(4, n4);
            System.out.println(l.getNode(10) + "->" + l.getNode(-1).next + " " + l.getNode(0) + "->" + l.getNode(0).next + " " + l.getNode(1) + "->" + l.getNode(1).next + " " + l.getNode(2) + "->" + l.getNode(2).next + " " + l.getNode(3) + "->" + l.getNode(3).next + " " + l.getNode(4) + "->" + l.getNode(4).next);
            l.addNode(0, n5);
            System.out.println(l.getNode(0) + "->" + l.getNode(0).next + " " + l.getNode(1) + "->" + l.getNode(1).next + " " + l.getNode(2) + "->" + l.getNode(2).next + " " + l.getNode(3) + "->" + l.getNode(3).next + " " + l.getNode(4) + "->" + l.getNode(4).next + " " + l.getNode(5) + "->" + l.getNode(5).next);
            l.addNode(3, n6);
            System.out.println(l.getNode(0) + "->" + l.getNode(0).next + " " + l.getNode(1) + "->" + l.getNode(1).next + " " + l.getNode(2) + "->" + l.getNode(2).next + " " + l.getNode(3) + "->" + l.getNode(3).next + " " + l.getNode(4) + "->" + l.getNode(4).next + " " + l.getNode(5) + "->" + l.getNode(5).next + " " + l.getNode(6) + "->" + l.getNode(6).next);
            l.addNode(7, n7);
            System.out.println(l.getNode(0) + "->" + l.getNode(0).next + " " + l.getNode(1) + "->" + l.getNode(1).next + " " + l.getNode(2) + "->" + l.getNode(2).next + " " + l.getNode(3) + "->" + l.getNode(3).next + " " + l.getNode(4) + "->" + l.getNode(4).next + " " + l.getNode(5) + "->" + l.getNode(5).next + " " + l.getNode(6) + "->" + l.getNode(6).next + " " + l.getNode(7) + "->" + l.getNode(7).next);
            System.out.println(l.getNode(0) + " " + l.getNode(7));
            System.out.println(l.getNode(0) + "->" + l.getNode(0).next + " " + l.getNode(1) + "->" + l.getNode(1).next + " " + l.getNode(2) + "->" + l.getNode(2).next + " " + l.getNode(3) + "->" + l.getNode(3).next);
            l.delNode(0);
            System.out.println(l.getNode(0) + "->" + l.getNode(0).next + " " + l.getNode(1) + "->" + l.getNode(1).next + " " + l.getNode(2) + "->" + l.getNode(2).next + " " + l.getNode(3) + "->" + l.getNode(3).next);
            l.delNode(2);
            System.out.println(l.getNode(0) + "->" + l.getNode(0).next + " " + l.getNode(1) + "->" + l.getNode(1).next + " " + l.getNode(2) + "->" + l.getNode(2).next + " " + l.getNode(3) + "->" + l.getNode(3).next);
            l.delNode(1);
            l.delNode(0);
            l.delNode(0);
            l.delNode(0);
            l.delNode(0);
            l.delNode(0);
            System.out.println(l.getNode(0));
            //System.out.println(l.getNode(0) + "->" + l.getNode(0).next + " " + l.getNode(1) + "->" + l.getNode(1).next + " " + l.getNode(2) + "->" + l.getNode(2).next +  " " + l.getNode(3) + "->" + l.getNode(3).next +  " " + l.getNode(4) + "->" + l.getNode(4).next + " " + l.getNode(5) + "->" + l.getNode(5).next + " " + l.getNode(6) + "->" + l.getNode(6).next + " " + l.getNode(7) + "->" + l.getNode(7).next);
*/
/*
            Office[] offices = new Office[4];
            offices[0] = new Office(45.5, 3);
            offices[1] = new Office(-15.0, 2);
            offices[2] = new Office(23.8, -4);
            offices[3] = new Office(12, 10);
            OfficeFloor of1 = new OfficeFloor(1);
            OfficeFloor of2 = new OfficeFloor(offices);
            //OfficeFloor of3=new OfficeFloor(2);
            //OfficeFloor of4=new OfficeFloor(4);
            //OfficeFloor of5=new OfficeFloor(7);
            System.out.println(of1.numberOfOffices() + " " + of2.numberOfOffices());
            System.out.println(of1.totalSquare() + " " + of2.totalSquare());
            System.out.println(of1.numberOfRooms() + " " + of2.numberOfRooms());
            System.out.println(of1.getOffice(10).getSquare() + " " + of2.getOffice(33).getSquare());
            of1.changeOffice(0, new Office(33.3, 3));
            of2.changeOffice(2, new Office(66.6, 6));
            System.out.println(of1.getOffice(0).getSquare() + " " + of2.getOffice(2).getSquare());
            of1.addOffice(-4, new Office());
            of2.addOffice(10, new Office());
            of1.delOffice(0);
            of2.delOffice(2);
            System.out.println(of1.numberOfOffices() + " " + of2.numberOfOffices());
            Office[] array1 = new Office[of1.numberOfOffices()];
            Office[] array2 = new Office[of2.numberOfOffices()];
            array1 = of1.arrayOfOffices();
            array2 = of2.arrayOfOffices();
            System.out.println();
            for (Office o : array1) {
                System.out.println(o.getNumberOfRooms() + " " + o.getSquare());
            }
            System.out.println();
            for (Office o : array2) {
                System.out.println(o.getNumberOfRooms() + " " + o.getSquare());
            }
            System.out.println(of1.getBestSpace().getSquare() + " " + of2.getBestSpace().getSquare());
*/
/*
            NodeDoubly n0 = new NodeDoubly();
            NodeDoubly n1 = new NodeDoubly();
            NodeDoubly n2 = new NodeDoubly();
            NodeDoubly n3 = new NodeDoubly();
            NodeDoubly n4 = new NodeDoubly();
            DoublyLinkedList l = new DoublyLinkedList();
            l.addNode(0, n0);
            System.out.println(l.getNode(-1) + " " + l.getNode(0) + "->" + l.getNode(0).next + "<-" + l.getNode(0).prev);
            l.addNode(0, n1);
            System.out.println(l.getNode(0) + "->" + l.getNode(0).next + " ->" + l.getNode(1).next);
            l.addNode(2, n2);
            System.out.println(l.getNode(0) + "->" + l.getNode(2).prev + " ->" + l.getNode(2) + "->" + l.getNode(2).next);
            l.delNode(0);
            System.out.println(l.getNode(0) + "->" + l.getNode(1) + " ->" + l.getNode(1).next + "->" + l.getNode(-1));
            l.delNode(1);
            System.out.println(l.getNode(-1) + "->" + l.getNode(0) + " ->" + l.getNode(0).next + "->" + l.getNode(-1).prev);
            l.delNode(0);
            System.out.println(l.getNode(-1));//голова
*/
/*
            Office[] offices = new Office[4];
            offices[0] = new Office(45.5, 3);
            offices[1] = new Office(15.0, 2);
            offices[2] = new Office(23.8, 4);
            offices[3] = new Office(12, 10);
            OfficeFloor of1 = new OfficeFloor(1);
            OfficeFloor of2 = new OfficeFloor(offices);
            OfficeFloor of3 = new OfficeFloor(2);
            OfficeFloor of4 = new OfficeFloor(4);
            OfficeFloor of5 = new OfficeFloor(7);
            OfficeFloor[] of = {of1, of2, of3, of4, of5};
            OfficeBuilding ob1 = new OfficeBuilding(of);
            int[] OF = {1, 2, 3, 7};
            OfficeBuilding ob2 = new OfficeBuilding(4, OF);
            System.out.println(ob1.getNumberOfFloors() + " " + ob2.getNumberOfFloors());
            System.out.println(ob1.getNumberOfSpaces() + " " + ob2.getNumberOfSpaces());
            System.out.println(ob1.getNumberOfRooms() + " " + ob2.getNumberOfRooms());
            System.out.println(ob1.getBestSpace().getSquare() + " " + ob2.getBestSpace().getSquare());
            System.out.println(ob1.getFloor(1).getBestSpace().getSquare() + " " + ob2.getArrayOfSpaces().length);
            ob1.deleteSpace(2);
            System.out.println(ob1.getArrayOfSpaces().length + " " + ob2.getNumberOfFloors());
            ob1.addSpace(0, new Office());
            System.out.println(ob1.getArrayOfSpaces().length + " " + ob2.getNumberOfFloors());
            Space[] array1;
            Space[] array2;
            array1 = ob1.getArrayOfSpaces();
            array2 = ob2.getArrayOfSpaces();
            for (Space o : array1) {
                System.out.print(o.getSquare() + " ");
            }
            System.out.println();
            for (Space o : array2) {
                System.out.print(o.getSquare() + " ");
            }
            System.out.println();
*/
/*
            Office off = new Office();
            off.setNumberOfRooms(2);
            off.setSquare(50.0);
            OfficeFloor of = new OfficeFloor(3);
            DwellingFloor df = new DwellingFloor(3);
            df.changeSpace(0, new Flat(250.0, 1));
            df.changeSpace(1, new Flat(250.0, 1));
            df.changeSpace(2, new Flat(250.0, 1));
            System.out.println(PlacementExchanger.isExchangedFloors(df, of));
            try {
                PlacementExchanger.exchangeFloorRooms(df, 0, of, 2);
            } catch (InexchangeableSpacesException ex) {
                System.out.println(ex.getMessage());
            }
*/
/*
            Office office = new Office();
            DwellingFloor df = new DwellingFloor(3);
            df.addSpace(0, office);
            System.out.println(df.getSpace(0).getSquare() + " " + df.getSpace(1).getSquare() + " " + df.getSpace(2).getSquare());
            Flat f = new Flat();
            OfficeFloor of = new OfficeFloor(3);
            of.addSpace(0, f);
            System.out.println(of.getSpace(0).getSquare() + " " + of.getSpace(1).getSquare() + " " + of.getSpace(2).getSquare());
*/
/*
            Office f4 = new Office(140.0, 2);
            Office f5 = new Office(60.0, 1);
            Office f6 = new Office(50.0, 1);
            Space[] flats3 = {f4, f5, f6};
            Space[] flats4 = {f4, f6, f5};
            Flat f1 = new Flat(150.0, 3);
            Flat f2 = new Flat(100.0, 2);
            Flat[] flats1 = {f1, f2};
            DwellingFloor df1 = new DwellingFloor(flats1);
            Flat f3 = new Flat(250.0, 3);
            Flat[] flats2 = {f3};
            DwellingFloor df2 = new DwellingFloor(flats2);
            OfficeFloor of = new OfficeFloor(flats3);
            OfficeFloor df6 = new OfficeFloor(flats4);
            SpaceIterator si = new SpaceIterator(df5);
            DwellingFloor df3 = new DwellingFloor(flats3);
            DwellingFloor df4 = new DwellingFloor(flats4);
            DwellingFloor[] df = {df1, df2, df3};
            Dwelling d = new Dwelling(df);
            //System.out.println(si.next().getSquare() + " " + si.next().getSquare() + " " + si.next().getSquare());
            //System.out.println(f4.toString() + " " + f5.toString());
            //System.out.println(df5.toString());
            //System.out.println(d.toString());
            //System.out.println(df6.equals(df5));
            //System.out.println(df2.hashCode());
            //System.out.println(f2.clone());//toString() сработал для Flat(не адрес вывел)
            System.out.println("Сам df5:" + df5.clone());
            System.out.println("Клон df5:" + df5.clone());
            //System.out.println(d.clone());
*/
/*
            try {
                FileOutputStream file = new FileOutputStream("out.bin");
                Buildings.outputBuilding(d, file);
                FileInputStream file1 = new FileInputStream("out.bin");
                System.out.println(Buildings.inputBuilding(file1).getSquareOfRooms());
                FileWriter file2 = new FileWriter("out.txt");
                Buildings.writeBuilding(d, file2);
                FileReader file3 = new FileReader("out.txt");
                System.out.println(Buildings.readBuilding(file3).getSpace(0).getSquare());
                Buildings.outputBuilding(d, System.out);
                //Buildings.inputBuilding(System.in);
                Flat f = new Flat(45.3, 6);
                //Flat f1;
                ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("object.bin"));
                out.writeObject(f);
                out.close();
                ObjectInputStream in = new ObjectInputStream(new FileInputStream("object.bin"));
                try {
                    f1 = (Flat) in.readObject();
                    in.close();
                    System.out.println(f1.getNumberOfRooms() + " " + f1.getSquare());
                } catch (ClassNotFoundException e) {
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        } catch (SpaceIndexOutOfBoundsException e) {
            System.out.println(e.getMsg());
        } catch (FloorIndexOutOfBoundsException e) {
            System.out.println(e.getMsg());
        } catch (InvalidSpaceAreaException e) {
            System.out.println(e.getMessage());
        } catch (InvalidRoomsCountException e) {
            System.out.println(e.getMessage());
        }
*/
/*
        Floor floor1 = new DwellingFloor(5);
        //Floor floor2=new OfficeFloor(5);
        //Floor floor3=new HotelFloor(5);
        Repairer r1 = new Repairer(floor1);
        //Repairer r2=new Repairer(floor2);
        //Repairer r3=new Repairer(floor3);
        Cleaner c1 = new Cleaner(floor1);
        //Cleaner c2=new Cleaner(floor2);
        //Cleaner c3=new Cleaner(floor3);
        //r1.interrupt();//ПОЧЕМУ interrupt() до r1.start(),а всё равно вып-ся?????
        r1.setPriority(MAX_PRIORITY);
        c1.setPriority(MIN_PRIORITY);
        r1.start();
        c1.start();
        //r2.start();
        //r3.start();
        //c2.start();
        //c3.start();
*/
/*
        Semaphore s = new Semaphore(1);
        SequentalCleaner r1 = new SequentalCleaner(floor1, s);
        SequentalRepairer r2 = new SequentalRepairer(floor1, s);
        Thread sc = new Thread((Runnable) r1);
        Thread sr = new Thread((Runnable) r2);
        sc.setPriority(MAX_PRIORITY);
        sr.setPriority(MIN_PRIORITY);
        sc.start();
        sr.start();
*/
        try {
            BufferedWriter bufferedWriter=new BufferedWriter(new FileWriter("1.txt"));
            String s="3 2 3 150.0 2 100.0 1 3 250.0 3 2 140.0 1 60.0 1 50.0\n\r";
            String s1="3 2 3 54.2 2 100.4 1 3 20.0 3 2 40.6 1 60.0 1 40.0" + "\n\r";
            String s2="3 2 3 10.0 4 100.0 1 3 20.1 3 2 140.0 1 60.3 1 50.0" + "\n\r";
            bufferedWriter.write(s);
            bufferedWriter.write(s1);
            bufferedWriter.write(s2);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
