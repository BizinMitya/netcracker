package netcracker;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Дмитрий
 */
public class SpaceIterator implements Iterator {
    private Floor floor;
    private final int size;
    private int temp;

    public SpaceIterator(Floor floor) {
        this.floor = floor;
        size = floor.numberOfSpaces();
        temp = 0;
    }

    @Override
    public boolean hasNext() {
        return (temp < size);
    }

    @Override
    public Space next() throws NoSuchElementException//если бы 4 итератора(Flat,Office,DwellingFloor,OfficeFloor),то оптимизация - хранить массив вместо списка(доступ быстрее) для Office и OfficeFloor
    {
        if (this.hasNext()) {
            temp++;
            return floor.getSpace(temp - 1);
        } else throw new NoSuchElementException();
    }

    @Override
    public void remove() {
    }
}
