package netcracker;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by bizin on 27.11.15.
 */
public class Reflection {
    public static void main(String []args) throws ClassNotFoundException, NoSuchMethodException {
        Class c=Class.forName(args[0]);
        Method method=c.getMethod(args[1],new Class [] {Double.TYPE});
        Double []values = new Double[args.length-2];
        for(int i=2;i<args.length;i++){
            values[i-2]=Double.valueOf(args[i]);
        }
        Object res = null;
        try {
            res = method.invoke(null,(Object) values);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.out.println(res.toString());
    }
}
