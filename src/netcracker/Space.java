package netcracker;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Дмитрий
 */
public interface Space {

    int getNumberOfRooms();

    double getSquare();

    void setNumberOfRooms(int numberOfRooms);

    void setSquare(double square);

    Object clone() throws CloneNotSupportedException;
}
