package netcracker;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Дмитрий
 */
public class InvalidRoomsCountException extends IllegalArgumentException {
    private String message;
    private int number;

    public InvalidRoomsCountException() {
        message = "Некорректные значения количества комнат в помещении!";
    }

    public InvalidRoomsCountException(int number) {
        message = "Некорректные значения количества комнат в помещении:" + number + "!";
        this.number = number;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
