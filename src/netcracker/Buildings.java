package netcracker;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import netcracker.buildings.dwelling.DwellingFactory;
import netcracker.buildings.threads.SynchronizedFloor;

import java.io.*;

/**
 * @author Дмитрий
 */
public class Buildings {
    public static BuildingFactory buildingFactory = new DwellingFactory();

    public static void setBuildingFactory(BuildingFactory buildingFactory) {
        Buildings.buildingFactory = buildingFactory;
    }

    public static Space createSpace(double area) {
        return buildingFactory.createSpace(area);
    }

    public static Space createSpace(int roomsCount, double area) {
        return buildingFactory.createSpace(roomsCount, area);
    }

    public static Floor createFloor(int spacesCount) {
        return buildingFactory.createFloor(spacesCount);
    }

    public static Floor createFloor(Space[] spaces) {
        return buildingFactory.createFloor(spaces);
    }

    public static Building createBuilding(int floorsCount, int[] spacesCounts) {
        return buildingFactory.createBuilding(floorsCount, spacesCounts);
    }

    public static Building createBuilding(Floor[] floors) {
        return buildingFactory.createBuilding(floors);
    }

    public static void outputBuilding(Building building, OutputStream out) throws IOException {
        BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(out));
        StringBuffer stringBuffer=new StringBuffer();
        stringBuffer.append(building.getNumberOfFloors());
        for (int i = 0; i < building.getNumberOfFloors(); i++) {
            stringBuffer.append(" " + building.getFloor(i).numberOfSpaces());
            for (int j = 0; j < building.getFloor(i).numberOfSpaces(); j++) {
                stringBuffer.append(" " + building.getFloor(i).getSpace(j).getNumberOfRooms());
                stringBuffer.append(" " + building.getFloor(i).getSpace(j).getSquare());
            }
        }
        stringBuffer.append("\n");
        bufferedWriter.write(stringBuffer.toString());
        bufferedWriter.flush();
    }

    public static Building inputBuilding(InputStream in) throws IOException {
        DataInputStream dataInputStream = new DataInputStream(in);
        int numberOfFloors = dataInputStream.readInt();
        int numberOfRooms;
        double square;
        int k;
        Floor[] floors = new Floor[numberOfFloors];
        for (int i = 0; i < numberOfFloors; i++) {
            k = dataInputStream.readInt();
            Space[] spaces = new Space[k];
            for (int j = 0; j < k; j++) {
                numberOfRooms = dataInputStream.readInt();
                square = dataInputStream.readDouble();
                spaces[j] = createSpace(numberOfRooms, square);
            }
            floors[i] = createFloor(spaces);
        }
        Building building = createBuilding(floors);
        return building;
    }

    public static void writeBuilding(Building building, Writer out) throws IOException {
        out.write(building.getNumberOfFloors() + " ");
        for (int i = 0; i < building.getNumberOfFloors(); i++) {
            out.write(building.getFloor(i).numberOfSpaces() + " ");
            for (int j = 0; j < building.getFloor(i).numberOfSpaces(); j++) {
                out.write(building.getFloor(i).getSpace(j).getNumberOfRooms() + " ");
                out.write(building.getFloor(i).getSpace(j).getSquare() + " ");
            }
        }
    }

    public static Building readBuilding(Reader in) throws IOException, NegativeArraySizeException {
        StreamTokenizer streamTokenizer = new StreamTokenizer(in);
        streamTokenizer.nextToken();
        int numberOfFloors = (int) streamTokenizer.nval;
        int numberOfRooms;
        double square;
        int k;
        Floor[] floors = new Floor[numberOfFloors];
        for (int i = 0; i < numberOfFloors; i++) {
            streamTokenizer.nextToken();
            k = (int) streamTokenizer.nval;
            Space[] spaces = new Space[k];
            for (int j = 0; j < k; j++) {
                streamTokenizer.nextToken();
                numberOfRooms = (int) streamTokenizer.nval;
                streamTokenizer.nextToken();
                square = streamTokenizer.nval;
                spaces[j] = createSpace(numberOfRooms, square);
            }
            floors[i] = createFloor(spaces);
        }
        Building building = createBuilding(floors);
        return building;
    }

    public Floor synchronizedFloor(Floor floor) {
        return new SynchronizedFloor(floor);
    }
}
