package netcracker;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.Serializable;

/**
 * @author Дмитрий
 */
public interface Building extends Serializable {

    void addSpace(int numberOfSpace, Space space);

    void changeOfSpace(int numberOfSpace, Space space);

    void changeOfFloor(int numberOfFloor, Floor floor);

    void deleteSpace(int numberOfSpace);

    Space[] getArrayOfSpaces();

    Floor[] getArrayOfFloors();

    Space getBestSpace();

    Space getSpace(int numberOfSpace);

    Floor getFloor(int numberOfFloor);

    int getNumberOfSpaces();

    int getNumberOfFloors();

    int getNumberOfRooms();

    double getSquareOfRooms();

    java.util.Iterator iterator();

    Object clone() throws CloneNotSupportedException;

}
