package netcracker;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Дмитрий
 */
public class SpaceIndexOutOfBoundsException extends IndexOutOfBoundsException {
    private String message;
    private int number;

    public SpaceIndexOutOfBoundsException(String message, int number) {
        this.message = message;
        this.number = number;
    }

    public SpaceIndexOutOfBoundsException(String message) {
        this.message = message;
        this.number = 0;
    }

    public String getMsg() {
        return message;
    }

    @Override
    public String getMessage() {
        return message + "Нет такого номера:" + number + "!";
    }
}
