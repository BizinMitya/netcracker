package netcracker;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Дмитрий
 */
public class PlacementExchanger {
    public static boolean isExchangedSpaces(Space space1, Space space2) {
        return (space1.getSquare() == space2.getSquare() && space1.getNumberOfRooms() == space2.getNumberOfRooms());
    }

    public static boolean isExchangedFloors(Floor floor1, Floor floor2) {
        return (floor1.totalSquare() == floor2.totalSquare() && floor1.numberOfSpaces() == floor2.numberOfSpaces());
    }

    public static void exchangeFloorRooms(Floor floor1, int index1, Floor floor2, int index2) throws InexchangeableSpacesException {
        if ((index1 >= 0 && index1 < floor1.numberOfSpaces()) && (index2 >= 0 && index2 < floor2.numberOfSpaces()) && isExchangedSpaces(floor1.getSpace(index1), floor2.getSpace(index2))) {
            Space space;
            space = floor1.getSpace(index1);
            floor1.changeSpace(index1, floor2.getSpace(index2));
            floor2.changeSpace(index2, space);
        } else throw new InexchangeableSpacesException();
    }

    public static void exchangeBuildingFloors(Building building1, int index1, Building building2, int index2) throws InexchangeableFloorsException {
        if ((index1 >= 0 && index1 < building1.getNumberOfFloors()) && (index2 >= 0 && index2 < building2.getNumberOfFloors()) && (isExchangedFloors(building1.getFloor(index1), building2.getFloor(index2)))) {
            Floor floor;
            floor = building1.getFloor(index1);
            building1.changeOfFloor(index1, building2.getFloor(index2));
            building2.changeOfFloor(index2, floor);
        } else throw new InexchangeableFloorsException();
    }
}
