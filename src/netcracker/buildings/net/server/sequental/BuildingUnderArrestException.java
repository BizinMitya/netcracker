package netcracker.buildings.net.server.sequental;

/**
 * Created by Дмитрий on 14.11.2015.
 */
public class BuildingUnderArrestException extends Exception {
    private String message;

    public BuildingUnderArrestException() {
        message = "Здание под арестом!";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
