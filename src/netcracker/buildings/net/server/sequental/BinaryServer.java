package netcracker.buildings.net.server.sequental;

import netcracker.Building;
import netcracker.Buildings;
import netcracker.buildings.dwelling.DwellingFactory;
import netcracker.buildings.dwelling.hotel.HotelFactory;
import netcracker.buildings.office.OfficeFactory;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

/**
 * Created by Дмитрий on 09.11.2015.
 */
public class BinaryServer {
    private static boolean isBuildingUnderArrest() {
        Random random = new Random();
        return random.nextInt(10) == 0;
    }

    private static String getPrice(Building building, String line) throws BuildingUnderArrestException {
        StringBuffer res = new StringBuffer();
        if (!isBuildingUnderArrest()) {
            if (line.equals("Hotel")) {
                res.append(2000 * building.getSquareOfRooms());
            }
            if (line.equals("Dwelling")) {
                res.append(1000 * building.getSquareOfRooms());
            }
            if (line.equals("OfficeBuilding")) {
                res.append(1500 * building.getSquareOfRooms());
            }
            res.append("\n");
            return res.toString();
        } else {
            throw new BuildingUnderArrestException();
        }
    }

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(27015)) {
            System.out.println("Сервер запущен и ожидает подключения клиента...");
            Socket client = serverSocket.accept();
            System.out.println("Клиент подключился к серверу");
            Building building;
            String line;
            String price = null;
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
                 BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()))) {
                while ((line = bufferedReader.readLine()) != null) {
                    System.out.println("Получено название здания: " + line);
                    switch (line) {
                        case "Dwelling":
                            Buildings.setBuildingFactory(new DwellingFactory());
                            break;
                        case "OfficeBuilding":
                            Buildings.setBuildingFactory(new OfficeFactory());
                            break;
                        case "Hotel":
                            Buildings.setBuildingFactory(new HotelFactory());
                            break;
                    }
                    building = Buildings.readBuilding(bufferedReader);
                    System.out.println("Получено здание: " + building.toString());
                    try {
                        price = getPrice(building, line);
                    } catch (BuildingUnderArrestException e) {
                        bufferedWriter.write("Здание под арестом!\n");
                        bufferedWriter.flush();
                        System.out.println("Здание под арестом!");
                        continue;
                    }
                    bufferedWriter.write(price);
                    bufferedWriter.flush();
                    System.out.println("Отправлена стоимость здания: " + price);
                }
                System.out.println("Прекращение запросов");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
