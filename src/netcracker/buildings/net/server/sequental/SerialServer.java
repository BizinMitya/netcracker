package netcracker.buildings.net.server.sequental;

import netcracker.Building;
import netcracker.Buildings;
import netcracker.buildings.dwelling.DwellingFactory;
import netcracker.buildings.dwelling.hotel.HotelFactory;
import netcracker.buildings.office.OfficeFactory;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

/**
 * Created by Дмитрий on 24.11.2015.
 */
public class SerialServer {
    private static boolean isBuildingUnderArrest() {
        Random random = new Random();
        return random.nextInt(10) == 0;
    }

    private static String getPrice(Building building, String line) throws BuildingUnderArrestException {
        StringBuffer res = new StringBuffer();
        if (!isBuildingUnderArrest()) {
            if (line.equals("Hotel")) {
                res.append(2000 * building.getSquareOfRooms());
            }
            if (line.equals("Dwelling")) {
                res.append(1000 * building.getSquareOfRooms());
            }
            if (line.equals("OfficeBuilding")) {
                res.append(1500 * building.getSquareOfRooms());
            }
            res.append("\n");
            return res.toString();
        } else {
            throw new BuildingUnderArrestException();
        }
    }

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(27015)) {
            System.out.println("Сервер запущен и ожидает подключения клиента...");
            Socket client = serverSocket.accept();
            System.out.println("Клиент подключился к серверу");
            Building building;
            String line;
            String price = null;
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(client.getOutputStream());
                 ObjectInputStream objectInputStream = new ObjectInputStream(client.getInputStream())) {
                while ((line = (String) objectInputStream.readObject()) != null) {
                    System.out.println("Получено название здания: " + line);
                    switch (line) {
                        case "Dwelling":
                            Buildings.setBuildingFactory(new DwellingFactory());
                            break;
                        case "OfficeBuilding":
                            Buildings.setBuildingFactory(new OfficeFactory());
                            break;
                        case "Hotel":
                            Buildings.setBuildingFactory(new HotelFactory());
                            break;
                    }
                    building = (Building) objectInputStream.readObject();
                    System.out.println("Получено здание: " + building.toString());
                    try {
                        price = getPrice(building, line);
                    } catch (BuildingUnderArrestException e) {
                        objectOutputStream.writeObject("Здание под арестом!\n");
                        objectOutputStream.flush();
                        System.out.println("Здание под арестом!\n");
                        continue;
                    }
                    objectOutputStream.writeObject(price);
                    objectOutputStream.flush();
                    System.out.println("Отправлена стоимость здания: " + price);
                }
                System.out.println("Прекращение запросов");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (EOFException e) {
                System.out.println("Прекращение запросов");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
