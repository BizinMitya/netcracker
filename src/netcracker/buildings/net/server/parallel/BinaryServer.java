package netcracker.buildings.net.server.parallel;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Дмитрий on 24.11.2015.
 */
public class BinaryServer {

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(27015)) {
            System.out.println("Сервер запущен и ожидает подключения клиентов...");
            while (true) {
                Socket client = serverSocket.accept();
                System.out.println("Клиент подключился к серверу");
                BinaryServerThread binaryServerThread = new BinaryServerThread(client);
                Thread thread = new Thread(binaryServerThread);
                thread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
