package netcracker.buildings.net.server.parallel;

import netcracker.Building;
import netcracker.Buildings;
import netcracker.buildings.dwelling.DwellingFactory;
import netcracker.buildings.dwelling.hotel.HotelFactory;
import netcracker.buildings.net.server.sequental.BuildingUnderArrestException;
import netcracker.buildings.office.OfficeFactory;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Random;

/**
 * Created by Дмитрий on 25.11.2015.
 */
public class SerialServerThread implements Runnable {
    private Socket client;

    public SerialServerThread(Socket client) {
        this.client = client;
    }

    private synchronized static boolean isBuildingUnderArrest() {
        Random random = new Random();
        return random.nextInt(10) == 0;
    }

    private synchronized static String getPrice(Building building, String line) throws BuildingUnderArrestException {
        StringBuffer res = new StringBuffer();
        if (!isBuildingUnderArrest()) {
            if (line.equals("Hotel")) {
                res.append(2000 * building.getSquareOfRooms());
            }
            if (line.equals("Dwelling")) {
                res.append(1000 * building.getSquareOfRooms());
            }
            if (line.equals("OfficeBuilding")) {
                res.append(1500 * building.getSquareOfRooms());
            }
            res.append("\n");
            return res.toString();
        } else {
            throw new BuildingUnderArrestException();
        }
    }

    @Override
    public void run() {
        Building building;
        String line;
        String price = null;
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(client.getOutputStream());
             ObjectInputStream objectInputStream = new ObjectInputStream(client.getInputStream())) {
            while (!(line = (String) objectInputStream.readObject()).equals("end")) {
                System.out.println("Получено название здания: " + line);
                switch (line) {
                    case "Dwelling":
                        Buildings.setBuildingFactory(new DwellingFactory());
                        break;
                    case "OfficeBuilding":
                        Buildings.setBuildingFactory(new OfficeFactory());
                        break;
                    case "Hotel":
                        Buildings.setBuildingFactory(new HotelFactory());
                        break;
                }
                building = (Building) objectInputStream.readObject();
                System.out.println("Получено здание: " + building.toString());
                try {
                    price = getPrice(building, line);
                } catch (BuildingUnderArrestException e) {
                    objectOutputStream.writeObject("Здание под арестом!\n");
                    objectOutputStream.flush();
                    System.out.println("Здание под арестом!\n");
                    continue;
                }
                objectOutputStream.writeObject(price);
                objectOutputStream.flush();
                System.out.println("Отправлена стоимость здания: " + price);
            }
            System.out.println("Прекращение запросов");
        } catch (EOFException e) {
            System.out.println("Прекращение запросов");
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }

    }
}
