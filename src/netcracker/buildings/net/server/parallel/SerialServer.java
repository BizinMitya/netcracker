package netcracker.buildings.net.server.parallel;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Дмитрий on 24.11.2015.
 */
public class SerialServer {
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(27015)) {
            System.out.println("Сервер запущен и ожидает подключения клиентов...");
            while (true) {
                Socket client = serverSocket.accept();
                System.out.println("Клиент подключился к серверу");
                SerialServerThread serialServerThread = new SerialServerThread(client);
                Thread thread = new Thread(serialServerThread);
                thread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
