package netcracker.buildings.net.client;

import netcracker.Building;
import netcracker.Buildings;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * @author Дмитрий
 */
public class BinaryClient {
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 27015)) {//ip и port сервера
            System.out.println("Установлено соединение с сервером");
            try (FileReader in1 = new FileReader("1.txt"); BufferedReader in2 = new BufferedReader(new FileReader("2.txt"));
                 BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                 FileWriter writer = new FileWriter("3.txt"); BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))) {
                System.out.println("Начинается чтение из файлов и отправка запросов на сервер...");
                String line2 = "";
                String line3 = "";
                Building building;
                while (in2.ready()) {
                    building = Buildings.readBuilding(in1);
                    System.out.println("Считано здание: " + building.toString());
                    line2 = in2.readLine() + "\n";
                    System.out.println("Считана строка: " + line2 + " из 2го файла");
                    bufferedWriter.write(line2);
                    bufferedWriter.flush();
                    System.out.println("Отправлена строка: " + line2 + " на сервер");
                    Buildings.outputBuilding(building, socket.getOutputStream());
                    System.out.println("Отправлено здание: " + building.toString() + " на сервер");
                    line3 = bufferedReader.readLine();
                    writer.write(line3 + "\n");
                    System.out.println("Получил стоимость/сообщение об аресте: " + line3 + " и записал в файл");
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
