package netcracker.buildings.net.client;

import netcracker.Building;
import netcracker.Buildings;

import java.io.*;
import java.net.Socket;

/**
 * Created by Дмитрий on 24.11.2015.
 */
public class SerialClient {
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 27015)) {//ip и port сервера
            System.out.println("Установлено соединение с сервером");
            try (FileReader in1 = new FileReader("1.txt"); BufferedReader in2 = new BufferedReader(new FileReader("2.txt"));
                 FileWriter writer = new FileWriter("3.txt"); ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                 ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream())) {
                System.out.println("Начинается чтение из файлов и отправка запросов на сервер...");
                String line2 = "";
                String line3 = "";
                Building building;
                while (in2.ready()) {
                    building = Buildings.readBuilding(in1);
                    System.out.println("Считано здание: " + building.toString());
                    line2 = in2.readLine();
                    System.out.println("Считана строка: " + line2 + " из 2го файла");
                    out.writeObject(line2);
                    out.flush();
                    System.out.println("Отправлена строка: " + line2 + " на сервер");
                    out.writeObject(building);
                    out.flush();
                    System.out.println("Отправлено здание: " + building.toString() + " на сервер");
                    line3 = (String) objectInputStream.readObject();
                    writer.write(line3);
                    System.out.println("Получил стоимость/сообщение об аресте: " + line3 + " и записал в файл");
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
