package netcracker.buildings.threads;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import netcracker.Floor;

/**
 * @author Дмитрий
 */
public class SequentalRepairer implements Runnable {
    private Floor floor;
    private Semaphore semaphore;

    public SequentalRepairer(Floor floor, Semaphore semaphore) {
        this.floor = floor;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < floor.numberOfSpaces(); i++) {
                semaphore.enter();
                System.out.println("Repairing space number " + i + " with total area " + floor.getSpace(i).getSquare() + " square meters");
                semaphore.leave();
            }
        } catch (InterruptedException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
