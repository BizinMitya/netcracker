package netcracker.buildings.threads;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import netcracker.Floor;

/**
 * @author bizin
 */
public class Cleaner extends Thread {
    private Floor floor;

    public Cleaner(Floor floor) {
        this.floor = floor;
    }

    @Override
    public void run() {
        for (int i = 0; i < floor.numberOfSpaces(); i++) {
            if (this.isInterrupted() == false) {
                System.out.println("Cleaning room number " + i + " with total area " + floor.getSpace(i).getSquare() + " square meters");
            } else {
                return;
            }
        }
    }
}
