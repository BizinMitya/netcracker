package netcracker.buildings.threads;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Дмитрий
 */
public class Semaphore {
    private static int count;
    private Thread sequentalCleaner;
    private Thread lastThread;

    public Semaphore(int count) {
        Semaphore.count = count;
        lastThread = null;
        sequentalCleaner = null;
    }

    public void setSequentalCleaner(Thread sequentalCleaner) {
        this.sequentalCleaner = sequentalCleaner;
        this.lastThread = sequentalCleaner;
    }

    public void init(int count) {
        Semaphore.count = count;
    }

    public synchronized void enter() throws InterruptedException {
        if (Thread.currentThread() != lastThread) {
            while (count <= 0) this.wait();
            count--;
        } else this.wait();
    }

    public synchronized void leave() {
        count++;
        lastThread = Thread.currentThread();
        this.notify();
    }
}
