package netcracker.buildings.threads;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import netcracker.Floor;

/**
 * @author Дмитрий
 */
public class SequentalCleaner implements Runnable {

    private Floor floor;
    private Semaphore semaphore;

    public SequentalCleaner(Floor floor, Semaphore semaphore) {
        this.floor = floor;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            semaphore.setSequentalCleaner(Thread.currentThread());
            for (int i = 0; i < floor.numberOfSpaces(); i++) {
                semaphore.enter();
                System.out.println("Cleaning room number " + i + " with total area " + floor.getSpace(i).getSquare() + " square meters");
                semaphore.leave();
            }
        } catch (InterruptedException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
