package netcracker.buildings.threads;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import netcracker.Floor;
import netcracker.Space;

import java.util.Iterator;


/**
 * @author bizin
 */
public class SynchronizedFloor implements Floor, Cloneable {
    private Floor floor;

    public SynchronizedFloor(Floor floor) {
        this.floor = floor;
    }

    @Override
    public synchronized void addSpace(int numberOfSpace, Space space) {
        floor.addSpace(numberOfSpace, space);
    }

    @Override
    public synchronized Space[] arrayOfSpaces() {
        return floor.arrayOfSpaces();
    }

    @Override
    public synchronized void changeSpace(int numberOfSpace, Space space) {
        floor.changeSpace(numberOfSpace, space);
    }

    @Override
    public synchronized void deleteSpace(int numberOfSpace) {
        floor.deleteSpace(numberOfSpace);
    }

    @Override
    public synchronized Space getBestSpace() {
        return floor.getBestSpace();
    }

    @Override
    public synchronized Space getSpace(int numberOfSpace) {
        return floor.getSpace(numberOfSpace);
    }

    @Override
    public synchronized int numberOfSpaces() {
        return floor.numberOfSpaces();
    }

    @Override
    public synchronized int totalRooms() {
        return floor.totalRooms();
    }

    @Override
    public synchronized double totalSquare() {
        return floor.totalSquare();
    }

    @Override
    public synchronized Iterator iterator() {
        return floor.iterator();
    }

    @Override
    public synchronized Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
