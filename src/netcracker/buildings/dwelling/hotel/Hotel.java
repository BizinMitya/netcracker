package netcracker.buildings.dwelling.hotel;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import netcracker.Floor;
import netcracker.Space;
import netcracker.buildings.dwelling.Dwelling;

/**
 * @author Дмитрий
 */
public class Hotel extends Dwelling {
    public Hotel(int numberOfFloors, int[] arrayOfSpaces) {
        super(numberOfFloors, arrayOfSpaces);
    }

    public Hotel(Floor[] floors) {
        super(floors);
    }

    public int getNumberOfStars() {
        int numberOfStars = 0;
        Floor[] floors = super.getArrayOfFloors();
        for (int i = 0; i < super.getNumberOfFloors(); i++) {
            if (((HotelFloor) floors[i]).getNumberOfStars() >= numberOfStars) {
                numberOfStars = ((HotelFloor) floors[i]).getNumberOfStars();
            }
        }
        return numberOfStars;
    }

    @Override
    public Space getBestSpace() {
        double max = 0.0;
        Floor[] floors = super.getArrayOfFloors();
        Space bestFlat = floors[0].getSpace(0);
        Space[] bestFlats = new Space[floors.length];
        for (int i = 0; i < floors.length; i++) {
            bestFlats[i] = floors[i].getBestSpace();
        }
        double a = 0;
        for (int i = 0; i < bestFlats.length; i++) {
            switch (((HotelFloor) getFloor(i)).getNumberOfStars()) {
                case 1:
                    a = 0.25;
                    break;
                case 2:
                    a = 0.5;
                    break;
                case 3:
                    a = 1;
                    break;
                case 4:
                    a = 1.25;
                    break;
                case 5:
                    a = 1.5;
                    break;
            }
            if (bestFlats[i].getSquare() * a >= max) {
                max = bestFlats[i].getSquare() * a;
                bestFlat = bestFlats[i];
            }
        }
        return bestFlat;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Hotel (");
        sb.append(this.getNumberOfStars());
        sb.append(", ");
        sb.append(this.getNumberOfFloors());
        for (int i = 0; i < this.getNumberOfFloors(); i++) {
            sb.append(", ");
            sb.append(this.getFloor(i).toString());
        }
        sb.append(')');
        return sb.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (!(object instanceof Hotel)) return false;
        Hotel h = (Hotel) object;
        if (h.getNumberOfFloors() != this.getNumberOfFloors()) return false;
        else {
            if (h.getNumberOfStars() != this.getNumberOfStars()) return false;
            else {
                boolean t = true;
                for (int i = 0; i < this.getNumberOfFloors(); i++) {
                    if (!this.getFloor(i).getSpace(i).equals(h.getFloor(i))) t = false;
                }
                return t;
            }
        }
    }

    @Override
    public int hashCode() {
        int hash = this.getNumberOfFloors() ^ this.getNumberOfStars();
        for (int i = 0; i < this.getNumberOfFloors(); i++) {
            hash = hash ^ this.getFloor(i).hashCode();
        }
        return hash;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();//вызывается метод clone() у Dwelling?
    }
}
