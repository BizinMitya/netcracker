package netcracker.buildings.dwelling.hotel;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import netcracker.BuildingFactory;
import netcracker.Floor;
import netcracker.Space;
import netcracker.buildings.dwelling.Flat;

/**
 * @author Дмитрий
 */
public class HotelFactory implements BuildingFactory {

    @Override
    public Flat createSpace(double area) {
        return new Flat(area);//?
    }

    @Override
    public Flat createSpace(int roomsCount, double area) {
        return new Flat(area, roomsCount);//?
    }

    @Override
    public HotelFloor createFloor(int spacesCount) {
        return new HotelFloor(spacesCount);
    }

    @Override
    public HotelFloor createFloor(Space[] spaces) {
        return new HotelFloor(spaces);
    }

    @Override
    public Hotel createBuilding(int floorsCount, int[] spacesCounts) {
        return new Hotel(floorsCount, spacesCounts);
    }

    @Override
    public Hotel createBuilding(Floor[] floors) {
        return new Hotel(floors);
    }

}
