package netcracker.buildings.dwelling.hotel;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import netcracker.Space;
import netcracker.buildings.dwelling.DwellingFloor;


/**
 * @author Дмитрий
 */
public class HotelFloor extends DwellingFloor {
    private int numberOfStars;
    public static final int NUMBER_OF_STARS = 1;

    public HotelFloor(int numberOfSpaces) {
        super(numberOfSpaces);
        numberOfStars = NUMBER_OF_STARS;
    }

    public HotelFloor(Space[] spaces) {
        super(spaces);
        numberOfStars = NUMBER_OF_STARS;
    }

    public int getNumberOfStars() {
        return numberOfStars;
    }

    public void changeNumberOfStars(int numberOfStars) {
        this.numberOfStars = numberOfStars;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("HotelFloor (");
        sb.append(this.getNumberOfStars());
        sb.append(", ");
        sb.append(this.numberOfSpaces());
        for (int i = 0; i < this.numberOfSpaces(); i++) {
            sb.append(", ");
            sb.append(this.getSpace(i).toString());
        }
        sb.append(')');
        return sb.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (!(object instanceof HotelFloor)) return false;
        HotelFloor hf = (HotelFloor) object;
        if (hf.numberOfSpaces() != this.numberOfSpaces()) return false;
        else {
            if (hf.getNumberOfStars() != this.getNumberOfStars()) return false;
            else {
                boolean t = true;
                for (int i = 0; i < this.numberOfSpaces(); i++) {
                    if (!this.getSpace(i).equals(hf.getSpace(i))) t = false;
                }
                return t;
            }
        }
    }

    @Override
    public int hashCode() {
        int hash = this.numberOfSpaces() ^ this.getNumberOfStars();
        for (int i = 0; i < this.numberOfSpaces(); i++) {
            hash = hash ^ this.getSpace(i).hashCode();
        }
        return hash;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();//вызывается clone() у DwellingFloor?(а поле numberOfStars - примитивное)
    }
}
