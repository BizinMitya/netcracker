package netcracker.buildings.dwelling;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import netcracker.*;

import java.io.Serializable;
import java.util.Iterator;

/**
 * @author Дмитрий
 */
public class Dwelling implements Building, Serializable, Cloneable {
    private Floor[] floors;

    public Dwelling(int numberOfFloors, int[] arrayOfFlats) {
        if (numberOfFloors == arrayOfFlats.length && numberOfFloors != 0) {
            this.floors = new Floor[numberOfFloors];
            for (int i = 0; i < floors.length; i++)//нумерация этажей с 0
            {
                this.floors[i] = new DwellingFloor(arrayOfFlats[i]);
            }
        } else if (numberOfFloors != arrayOfFlats.length || numberOfFloors == 0 || arrayOfFlats.length == 0)
            throw new FloorIndexOutOfBoundsException("Неправильное число этажей!\n");
    }

    public Dwelling(Floor[] floors) {
        if (floors.length != 0) {
            this.floors = new Floor[floors.length];
            for (int i = 0; i < floors.length; i++) {
                this.floors[i] = floors[i];
            }
        } else throw new FloorIndexOutOfBoundsException("Задан массив нулевой длины!\n");
    }

    @Override
    public int getNumberOfFloors() {
        return floors.length;
    }

    @Override
    public int getNumberOfSpaces() {
        int sum = 0;
        if (floors.length != 0) {
            for (Floor df : floors) {
                sum += df.numberOfSpaces();
            }
            return sum;
        } else throw new FloorIndexOutOfBoundsException("Массив пуст!\n");
    }

    @Override
    public double getSquareOfRooms() {
        double sum = 0.0;
        if (floors.length != 0) {
            for (Floor df : floors) {
                sum += df.totalSquare();
            }
            return sum;
        } else throw new FloorIndexOutOfBoundsException("Массив пуст!\n");
    }

    @Override
    public int getNumberOfRooms() {
        int sum = 0;
        if (floors.length != 0) {
            for (Floor df : floors) {
                sum += df.totalRooms();
            }
            return sum;
        } else throw new FloorIndexOutOfBoundsException("Массив пуст!\n");
    }

    @Override
    public Floor[] getArrayOfFloors() {
        if (floors.length != 0) {
            return floors;
        } else throw new FloorIndexOutOfBoundsException("Массив пуст!\n");
    }

    @Override
    public Floor getFloor(int numberOfFloor) {
        if (floors.length == 0) {
            throw new FloorIndexOutOfBoundsException("Массив пуст!\n", numberOfFloor);
        } else {
            if (numberOfFloor >= 0 && numberOfFloor < floors.length) {
                return floors[numberOfFloor];
            } else throw new FloorIndexOutOfBoundsException("Неправильный номер этажа!\n", numberOfFloor);
        }
    }

    @Override
    public void changeOfFloor(int numberOfFloor, Floor dwellingFloor) {
        if (floors.length == 0) {
            throw new FloorIndexOutOfBoundsException("Массив пуст!\n", numberOfFloor);
        } else {
            if (numberOfFloor >= 0 && numberOfFloor < floors.length) {
                this.floors[numberOfFloor] = dwellingFloor;
            } else
                throw new FloorIndexOutOfBoundsException("Выход за границы номеров этажей!\nЭтаж не изменён!\n", numberOfFloor);
        }
    }

    @Override
    public Space getSpace(int numberOfFlat) {
        if (floors.length == 0) {
            throw new FloorIndexOutOfBoundsException("Массив пуст!\n");
        } else {
            if (getNumberOfSpaces() >= numberOfFlat) {
                int l = 0, r = floors[0].numberOfSpaces() - 1, res = numberOfFlat, i;
                for (i = 0; i < floors.length - 1; i++) {
                    if (numberOfFlat >= l && numberOfFlat <= r) {
                        break;
                    } else {
                        l += floors[i].numberOfSpaces();
                        r += floors[i + 1].numberOfSpaces();
                        res -= floors[i].numberOfSpaces();
                    }
                }
                return floors[i].getSpace(res);
            } else throw new FloorIndexOutOfBoundsException("Выход за границы этажей здания!\n", numberOfFlat);
        }
    }

    @Override
    public void changeOfSpace(int numberOfFlat, Space flat) {
        if (floors.length == 0) {
            throw new FloorIndexOutOfBoundsException("Массив пуст!\n");
        } else {
            if (getNumberOfSpaces() >= numberOfFlat) {
                int l = 0, r = floors[0].numberOfSpaces() - 1, res = numberOfFlat, i;
                for (i = 0; i < floors.length - 1; i++) {
                    if (numberOfFlat >= l && numberOfFlat <= r) {
                        break;
                    } else {
                        l += floors[i].numberOfSpaces();
                        r += floors[i + 1].numberOfSpaces();
                        res -= floors[i].numberOfSpaces();
                    }
                }
                this.floors[i].changeSpace(res, flat);
            } else
                throw new FloorIndexOutOfBoundsException("Выход за границы этажей здания!Квартира не изменена!\n", numberOfFlat);
        }
    }

    @Override
    public void addSpace(int numberOfFlat, Space flat) {
        if (floors.length == 0) {
            throw new FloorIndexOutOfBoundsException("Массив пуст!\n");
        } else {
            if (getNumberOfSpaces() >= numberOfFlat) {
                int l = 0, r = floors[0].numberOfSpaces() - 1, res = numberOfFlat, i;
                for (i = 0; i < floors.length - 1; i++) {
                    if (numberOfFlat >= l && numberOfFlat <= r) {
                        break;
                    } else {
                        l += floors[i].numberOfSpaces();
                        r += floors[i + 1].numberOfSpaces();
                        res -= floors[i].numberOfSpaces();
                    }
                }
                this.floors[i].addSpace(res, flat);
            } else
                throw new FloorIndexOutOfBoundsException("Выход за границы этажей здания!Квартира не добавлена!\n", numberOfFlat);
        }
    }

    @Override
    public void deleteSpace(int numberOfFlat) {
        if (floors.length == 0) {
            throw new FloorIndexOutOfBoundsException("Массив пуст!\n");
        } else {
            if (getNumberOfSpaces() >= numberOfFlat) {
                int l = 0, r = floors[0].numberOfSpaces() - 1, res = numberOfFlat, i;
                for (i = 0; i < floors.length - 1; i++) {
                    if (numberOfFlat >= l && numberOfFlat <= r) {
                        break;
                    } else {
                        l += floors[i].numberOfSpaces();
                        r += floors[i + 1].numberOfSpaces();
                        res -= floors[i].numberOfSpaces();
                    }
                }
                this.floors[i].deleteSpace(res);
            } else
                throw new FloorIndexOutOfBoundsException("Выход за границы этажей здания!Квартира не удалена!\n", numberOfFlat);
        }
    }

    @Override
    public Space getBestSpace() {
        double max = 0.0;
        Space maxFlat = floors[0].getSpace(0);
        if (floors.length > 0) {
            Space[] maxFlats = new Space[floors.length];
            for (int i = 0; i < floors.length; i++) {
                maxFlats[i] = floors[i].getBestSpace();
            }
            for (int i = 0; i < maxFlats.length; i++) {
                if (maxFlats[i].getSquare() >= max) {
                    max = maxFlats[i].getSquare();
                    maxFlat = maxFlats[i];
                }
            }
            return maxFlat;
        } else throw new FloorIndexOutOfBoundsException("Массив этажей пуст!\n");
    }

    @Override
    public Space[] getArrayOfSpaces() {
        Space[] array;
        if (getNumberOfSpaces() > 0) {
            array = new Space[getNumberOfSpaces()];
            int k = 0;
            for (int i = 0; i < floors.length; i++) {
                for (int j = 0; j < floors[i].numberOfSpaces(); j++) {
                    array[k] = floors[i].getSpace(j);
                    k++;
                }
            }
            Space maxFlat;
            int imax;
            for (int i = 0; i < array.length - 1; i++) {
                maxFlat = array[i];
                imax = i;
                for (int j = i + 1; j < array.length; j++) {
                    if (array[j].getSquare() > maxFlat.getSquare()) {
                        maxFlat = array[j];
                        imax = j;
                    }
                }
                array[imax] = array[i];
                array[i] = maxFlat;
            }
            return array;
        } else throw new FloorIndexOutOfBoundsException("Массив этажей пуст!\n");
    }

    @Override
    public Iterator iterator() {
        return new FloorIterator(this);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Dwelling (");
        sb.append(this.getNumberOfFloors());
        for (int i = 0; i < this.getNumberOfFloors(); i++) {
            sb.append(", ");
            sb.append(this.getFloor(i).toString());
        }
        sb.append(')');
        return sb.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (!(object instanceof Dwelling)) return false;
        Dwelling d = (Dwelling) object;
        if (d.getNumberOfFloors() != this.getNumberOfFloors()) return false;
        else {
            boolean t = true;
            for (int i = 0; i < this.getNumberOfFloors(); i++) {
                if (!this.getFloor(i).equals(d.getFloor(i))) t = false;
            }
            return t;
        }
    }

    @Override
    public int hashCode() {
        int hash = this.getNumberOfFloors();
        for (int i = 0; i < this.getNumberOfFloors(); i++) {
            hash = hash ^ this.getFloor(i).hashCode();
        }
        return hash;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Dwelling d = (Dwelling) super.clone();
        d.floors = floors.clone();
        for (int i = 0; i < floors.length; i++) {
            d.floors[i] = (Floor) this.floors[i].clone();
        }
        return d;
    }
}
