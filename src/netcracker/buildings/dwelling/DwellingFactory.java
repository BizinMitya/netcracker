package netcracker.buildings.dwelling;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import netcracker.BuildingFactory;
import netcracker.Floor;
import netcracker.Space;

/**
 * @author Дмитрий
 */
public class DwellingFactory implements BuildingFactory {

    @Override
    public Flat createSpace(double area) {
        return new Flat(area);
    }

    @Override
    public Flat createSpace(int roomsCount, double area) {
        return new Flat(area, roomsCount);
    }

    @Override
    public DwellingFloor createFloor(int spacesCount) {
        return new DwellingFloor(spacesCount);
    }

    @Override
    public DwellingFloor createFloor(Space[] spaces) {
        return new DwellingFloor(spaces);
    }

    @Override
    public Dwelling createBuilding(int floorsCount, int[] spacesCounts) {
        return new Dwelling(floorsCount, spacesCounts);
    }

    @Override
    public Dwelling createBuilding(Floor[] floors) {
        return new Dwelling(floors);
    }

}
