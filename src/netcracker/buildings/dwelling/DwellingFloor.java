package netcracker.buildings.dwelling;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import netcracker.Floor;
import netcracker.Space;
import netcracker.SpaceIndexOutOfBoundsException;
import netcracker.SpaceIterator;

import java.io.Serializable;
import java.util.Iterator;

/**
 * @author Дмитрий
 */
public class DwellingFloor implements Floor, Serializable, Cloneable {
    private Space[] flats;

    public DwellingFloor(int numberOfFlats) {
        if (numberOfFlats > 0) {
            flats = new Space[numberOfFlats];
            for (int i = 0; i < numberOfFlats; i++) {
                flats[i] = new Flat();
            }
        } else throw new SpaceIndexOutOfBoundsException("Неправильное количество квартир!\n", numberOfFlats);
    }

    public DwellingFloor(Space[] flats) {
        if (flats.length != 0) {
            this.flats = new Space[flats.length];
            for (int i = 0; i < flats.length; i++) {
                this.flats[i] = flats[i];
            }
        } else throw new SpaceIndexOutOfBoundsException("Передан массив нулевой длины!\n");
    }

    @Override
    public int numberOfSpaces() {
        return flats.length;
    }

    @Override
    public double totalSquare() {
        double sum = 0.0;
        if (flats.length == 0) {
            throw new SpaceIndexOutOfBoundsException("Выход за границы номеров квартир!\nМассив пуст!\n");
        } else {
            for (Space flat : flats) {
                sum += flat.getSquare();
            }
            return sum;
        }
    }

    @Override
    public int totalRooms() {
        int sum = 0;
        if (flats.length == 0) {
            throw new SpaceIndexOutOfBoundsException("Выход за границы номеров квартир!\nМассив пуст!\n");
        } else {
            for (Space flat : flats) {
                sum += flat.getNumberOfRooms();
            }
            return sum;
        }
    }

    @Override
    public Space[] arrayOfSpaces() {
        if (flats.length == 0) {
            throw new SpaceIndexOutOfBoundsException("Выход за границы номеров квартир!\nМассив пуст!\n");
        } else {
            return flats;
        }
    }

    @Override
    public Space getSpace(int numberOfFlat) {
        if (flats.length == 0) {
            throw new SpaceIndexOutOfBoundsException("Массив пуст!\n", numberOfFlat);
        } else {
            if (numberOfFlat >= 0 && numberOfFlat < flats.length) {
                return flats[numberOfFlat];
            } else throw new SpaceIndexOutOfBoundsException("Выход за границы номеров квартир!\n", numberOfFlat);
        }
    }

    @Override
    public void changeSpace(int numberOfFlat, Space flat)//исправил(только внутри границ)
    {
        if (flats.length == 0) {
            throw new SpaceIndexOutOfBoundsException("Массив пуст!\n", numberOfFlat);
        } else {
            if (numberOfFlat >= 0 && numberOfFlat < flats.length) {
                this.flats[numberOfFlat] = flat;
            } else
                throw new SpaceIndexOutOfBoundsException("Выход за границы номеров квартир!\nКвартира не изменена!\n", numberOfFlat);
        }
    }

    @Override
    public void addSpace(int numberOfFlat, Space flat)//исправил(внутри границ или в конец)
    {
        if (flats.length == 0) {
            throw new SpaceIndexOutOfBoundsException("Массив пуст!\n", numberOfFlat);
        } else {
            if (numberOfFlat >= 0 && numberOfFlat <= flats.length) {
                Space[] temp = new Space[flats.length + 1];
                for (int i = 0; i < numberOfFlat; i++) {
                    temp[i] = flats[i];
                }
                temp[numberOfFlat] = flat;
                for (int i = numberOfFlat + 1; i < flats.length + 1; i++) {
                    temp[i] = flats[i - 1];
                }
                this.flats = temp;
            } else
                throw new SpaceIndexOutOfBoundsException("Выход за границы номеров квартир!\nКвартира не добавлена!\n", numberOfFlat);
        }
    }

    @Override
    public void deleteSpace(int numberOfFlat) {
        if (flats.length == 0) {
            throw new SpaceIndexOutOfBoundsException("Массив пуст!\n", numberOfFlat);
        } else {
            if (numberOfFlat >= 0 && numberOfFlat < flats.length) {
                Space[] temp = new Space[flats.length - 1];
                for (int i = 0; i < numberOfFlat; i++) {
                    temp[i] = flats[i];
                }
                for (int i = numberOfFlat + 1; i < flats.length; i++) {
                    temp[i - 1] = flats[i];
                }
                this.flats = temp;
            } else
                throw new SpaceIndexOutOfBoundsException("Выход за границы номеров квартир!\nКвартира не удалена!\n", numberOfFlat);
        }
    }

    @Override
    public Space getBestSpace() {
        double max = 0.0;
        Space maxFlat = flats[0];
        if (flats.length > 0) {
            for (Space flat : flats) {
                if (flat.getSquare() >= max) {
                    max = flat.getSquare();
                    maxFlat = flat;
                }
            }
            return maxFlat;
        } else throw new SpaceIndexOutOfBoundsException("Массив квартир пуст!\n");
    }

    @Override
    public Iterator iterator() {
        return new SpaceIterator(this);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("DwellingFloor (");
        sb.append(this.numberOfSpaces());
        for (int i = 0; i < this.numberOfSpaces(); i++) {
            sb.append(", ");
            sb.append(this.getSpace(i).toString());
        }
        sb.append(')');
        return sb.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (!(object instanceof DwellingFloor)) return false;
        DwellingFloor df = (DwellingFloor) object;
        if (df.numberOfSpaces() != this.numberOfSpaces()) return false;
        else {
            boolean t = true;
            for (int i = 0; i < this.numberOfSpaces(); i++) {
                if (!this.getSpace(i).equals(df.getSpace(i))) t = false;
            }
            return t;
        }
    }

    @Override
    public int hashCode() {
        int hash = this.numberOfSpaces();
        for (int i = 0; i < this.numberOfSpaces(); i++) {
            hash = hash ^ this.getSpace(i).hashCode();
        }
        return hash;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DwellingFloor df = (DwellingFloor) super.clone();
        df.flats = this.flats.clone();
        for (int i = 0; i < flats.length; i++) {
            df.flats[i] = (Space) this.flats[i].clone();
        }
        return df;
    }
}
