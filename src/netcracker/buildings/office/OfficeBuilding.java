package netcracker.buildings.office;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import netcracker.*;

import java.io.Serializable;
import java.util.Iterator;

/**
 * @author Дмитрий
 */
public class OfficeBuilding implements Building, Serializable, Cloneable {
    private DoublyLinkedList floors;

    public OfficeBuilding(int numberOfFloors, int[] offices) {
        if (numberOfFloors == offices.length && numberOfFloors != 0) {
            floors = new DoublyLinkedList();
            for (int i = 0; i < numberOfFloors; i++)//numberOfFloors должно равняться offices.length
            {
                NodeDoubly node = new NodeDoubly(new OfficeFloor(offices[i]));
                this.floors.addNode(i, node);
            }
        } else if (numberOfFloors != offices.length || numberOfFloors == 0 || offices.length == 0)
            throw new FloorIndexOutOfBoundsException("Неправильное число этажей!\n");
    }

    public OfficeBuilding(Floor[] officeFloor) {
        if (officeFloor.length != 0) {
            floors = new DoublyLinkedList();
            for (int i = 0; i < officeFloor.length; i++) {
                this.floors.addNode(i, new NodeDoubly(officeFloor[i]));
            }
        } else throw new FloorIndexOutOfBoundsException("Задан массив нулевой длины!\n");
    }

    @Override
    public int getNumberOfFloors() {
        int i = 0;
        NodeDoubly head = floors.getNode(-1);
        NodeDoubly t = head;
        while (t.next != head) {
            i++;
            t = t.next;
        }
        return i;
    }

    @Override
    public int getNumberOfSpaces() {
        NodeDoubly head = floors.getNode(-1);
        NodeDoubly t = head;
        int sum = 0;
        if (t.next != head) {
            t = t.next;
            while (t != head) {
                sum += t.floor.numberOfSpaces();
                t = t.next;
            }
            return sum;
        } else throw new FloorIndexOutOfBoundsException("Список пуст!\n");
    }

    @Override
    public double getSquareOfRooms() {
        NodeDoubly head = floors.getNode(-1);
        NodeDoubly t = head;
        double sum = 0.0;
        if (t.next == head) {
            throw new FloorIndexOutOfBoundsException("Список пуст!\n");
        } else {
            t = t.next;
            while (t != head) {
                sum += t.floor.totalSquare();
                t = t.next;
            }
            return sum;
        }
    }

    @Override
    public int getNumberOfRooms() {
        NodeDoubly head = floors.getNode(-1);
        NodeDoubly t = head;
        int sum = 0;
        if (t.next == head) {
            throw new FloorIndexOutOfBoundsException("Список пуст!\n");
        } else {
            t = t.next;
            while (t != head) {
                sum += t.floor.totalRooms();
                t = t.next;
            }
            return sum;
        }
    }

    @Override
    public Floor[] getArrayOfFloors() {
        int k = this.getNumberOfFloors();
        Floor[] array = new Floor[k];
        if (k == 0) {
            throw new FloorIndexOutOfBoundsException("Список пуст!\n");
        } else {
            NodeDoubly head = floors.getNode(-1);
            NodeDoubly t = head.next;
            int i = 0;
            while (t != head) {
                array[i] = t.floor;
                i++;
                t = t.next;
            }
            return array;
        }
    }

    @Override
    public Floor getFloor(int numberOfFloor) {
        if (getNumberOfFloors() == 0) {
            throw new FloorIndexOutOfBoundsException("Список пуст!\n", numberOfFloor);
        } else {
            if (numberOfFloor >= 0 && numberOfFloor < getNumberOfFloors()) {
                return floors.getNode(numberOfFloor).floor;
            } else throw new FloorIndexOutOfBoundsException("Неправильный номер этажа!\n", numberOfFloor);
        }
    }

    @Override
    public void changeOfFloor(int numberOfFloor, Floor officeFloor) {
        this.floors.getNode(numberOfFloor).floor = officeFloor;
    }

    @Override
    public Space getSpace(int numberOfOffice) {
        NodeDoubly head = floors.getNode(-1);
        NodeDoubly t = head;
        if (getNumberOfFloors() == 0) {
            throw new FloorIndexOutOfBoundsException("Список пуст!\n", numberOfOffice);
        } else {
            t = t.next;
            int l = 0, r = t.floor.numberOfSpaces() - 1, res = numberOfOffice, i = 0;
            while (t != head) {
                if (numberOfOffice >= l && numberOfOffice <= r) {
                    break;
                } else {
                    if (t.next == head) {
                        throw new FloorIndexOutOfBoundsException("Неправильный номер офиса!\n", numberOfOffice);
                    } else {
                        l += t.floor.numberOfSpaces();
                        r += t.next.floor.numberOfSpaces();
                        res -= t.floor.numberOfSpaces();
                    }
                }
                t = t.next;
                i++;
            }
            return floors.getNode(i).floor.getSpace(res);
        }
    }

    @Override
    public void changeOfSpace(int numberOfOffice, Space office) {
        NodeDoubly head = floors.getNode(-1);
        NodeDoubly t = head;
        if (getNumberOfFloors() == 0) {
            throw new FloorIndexOutOfBoundsException("Список пуст!Офис не изменён!\n", numberOfOffice);
        } else {
            t = t.next;
            int l = 0, r = t.floor.numberOfSpaces() - 1, res = numberOfOffice, i = 0;
            while (t != head) {
                if (numberOfOffice >= l && numberOfOffice <= r) {
                    break;
                } else {
                    if (t.next == head) {
                        throw new FloorIndexOutOfBoundsException("Неправильный номер офиса!Офис не изменён!\n", numberOfOffice);
                    } else {
                        l += t.floor.numberOfSpaces();
                        r += t.next.floor.numberOfSpaces();
                        res -= t.floor.numberOfSpaces();
                    }
                }
                t = t.next;
                i++;
            }
            floors.getNode(i).floor.changeSpace(numberOfOffice, office);
        }
    }

    @Override
    public void addSpace(int numberOfOffice, Space office) {
        NodeDoubly head = floors.getNode(-1);
        NodeDoubly t = head;
        if (getNumberOfFloors() == 0) {
            throw new FloorIndexOutOfBoundsException("Список пуст!Офис не добавлен!\n", numberOfOffice);
        } else {
            t = t.next;
            int l = 0, r = t.floor.numberOfSpaces() - 1, res = numberOfOffice, i = 0;
            while (t != head) {
                if (numberOfOffice >= l && numberOfOffice <= r) {
                    break;
                } else {
                    if (t.next == head) {
                        throw new FloorIndexOutOfBoundsException("Неправильный номер офиса!Офис не добавлен!\n", numberOfOffice);
                    } else {
                        l += t.floor.numberOfSpaces();
                        r += t.next.floor.numberOfSpaces();
                        res -= t.floor.numberOfSpaces();
                    }
                }
                t = t.next;
                i++;
            }
            floors.getNode(i).floor.addSpace(numberOfOffice, office);
        }
    }

    @Override
    public void deleteSpace(int numberOfOffice) {
        NodeDoubly head = floors.getNode(-1);
        NodeDoubly t = head;
        if (getNumberOfFloors() == 0) {
            throw new FloorIndexOutOfBoundsException("Список пуст!Офис не удалён!\n", numberOfOffice);
        } else {
            t = t.next;
            int l = 0, r = t.floor.numberOfSpaces() - 1, res = numberOfOffice, i = 0;
            while (t != head) {
                if (numberOfOffice >= l && numberOfOffice <= r) {
                    break;
                } else {
                    if (t.next == head) {
                        throw new FloorIndexOutOfBoundsException("Неправильный номер офиса!Офис не удалён!\n", numberOfOffice);
                    } else {
                        l += t.floor.numberOfSpaces();
                        r += t.next.floor.numberOfSpaces();
                        res -= t.floor.numberOfSpaces();
                    }
                }
                t = t.next;
                i++;
            }
            floors.getNode(i).floor.deleteSpace(numberOfOffice);
        }
    }

    @Override
    public Space getBestSpace() {
        NodeDoubly head = floors.getNode(-1);
        NodeDoubly t = head;
        if (getNumberOfFloors() == 0) {
            throw new FloorIndexOutOfBoundsException("Список пуст!n");
        } else {
            t = t.next;
            double max = t.floor.getBestSpace().getSquare();
            Space maxOffice = floors.getNode(0).floor.getSpace(0);
            maxOffice = t.floor.getBestSpace();
            while (t != head) {
                if (t.floor.getBestSpace().getSquare() >= max) {
                    max = t.floor.getBestSpace().getSquare();
                    maxOffice = t.floor.getBestSpace();
                }
                t = t.next;
            }
            return maxOffice;
        }
    }

    @Override
    public Space[] getArrayOfSpaces() {
        int numberOfOffices = this.getNumberOfSpaces();
        NodeDoubly head = floors.getNode(-1);
        NodeDoubly t = head;
        if (getNumberOfFloors() == 0) {
            throw new FloorIndexOutOfBoundsException("Список пуст!\n");
        } else {
            Space[] array = new Space[numberOfOffices];
            int k = 0;
            t = t.next;
            while (t != head) {
                for (int i = 0; i < t.floor.numberOfSpaces(); i++) {
                    array[k] = t.floor.getSpace(i);
                    k++;
                }
                t = t.next;
            }
            Space maxOffice;
            int imax;
            for (int i = 0; i < array.length - 1; i++) {
                maxOffice = array[i];
                imax = i;
                for (int j = i + 1; j < array.length; j++) {
                    if (array[j].getSquare() > maxOffice.getSquare()) {
                        maxOffice = array[j];
                        imax = j;
                    }
                }
                array[imax] = array[i];
                array[i] = maxOffice;
            }
            return array;
        }
    }

    @Override
    public Iterator iterator() {
        return new FloorIterator(this);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("OfficeBuilding (");
        sb.append(this.getNumberOfFloors());
        for (int i = 0; i < this.getNumberOfFloors(); i++) {
            sb.append(", ");
            sb.append(this.getFloor(i).toString());
        }
        sb.append(')');
        return sb.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (!(object instanceof OfficeBuilding)) return false;
        OfficeBuilding ob = (OfficeBuilding) object;
        if (ob.getNumberOfFloors() != this.getNumberOfFloors()) return false;
        else {
            boolean t = true;
            Floor[] f1 = this.getArrayOfFloors();
            Floor[] f2 = ob.getArrayOfFloors();
            for (int i = 0; i < this.getNumberOfFloors(); i++) {
                if (!f1[i].equals(f2[i])) t = false;
            }
            return t;
        }
    }

    @Override
    public int hashCode() {
        int hash = this.getNumberOfFloors();
        for (int i = 0; i < this.getNumberOfFloors(); i++) {
            hash = hash ^ this.getFloor(i).hashCode();
        }
        return hash;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        OfficeBuilding d = (OfficeBuilding) super.clone();
        d.floors = (DoublyLinkedList) floors.clone();
        return d;
    }
}
