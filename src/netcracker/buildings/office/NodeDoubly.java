package netcracker.buildings.office;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import netcracker.Floor;

/**
 * @author Дмитрий
 */
public class NodeDoubly {
    public NodeDoubly next;
    public NodeDoubly prev;
    public Floor floor;

    public NodeDoubly() {
        this.next = null;
        this.prev = null;
        this.floor = null;
    }

    public NodeDoubly(Floor floor) {
        this.floor = floor;
        this.next = null;
        this.prev = null;
    }

}
