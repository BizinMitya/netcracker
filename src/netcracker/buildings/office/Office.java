package netcracker.buildings.office;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import netcracker.InvalidRoomsCountException;
import netcracker.InvalidSpaceAreaException;
import netcracker.Space;

import java.io.Serializable;


/**
 * @author bizin
 */
public class Office implements Space, Serializable, Cloneable {
    public static final int NUMBER_OF_ROOMS = 1;
    public static final double SQUARE = 250.0;
    private int numberOfRooms;
    private double square;

    public Office() {
        this.numberOfRooms = NUMBER_OF_ROOMS;
        this.square = SQUARE;
    }

    public Office(double square) {
        if (square >= 0) {
            this.numberOfRooms = NUMBER_OF_ROOMS;
            this.square = square;
        } else throw new InvalidSpaceAreaException(square);
    }

    public Office(double square, int numberOfRooms) {
        if (square < 0) throw new InvalidSpaceAreaException(square);
        else if (numberOfRooms < 0) throw new InvalidRoomsCountException(numberOfRooms);
        else if (square >= 0 && numberOfRooms >= 0) {
            this.numberOfRooms = numberOfRooms;
            this.square = square;
        }
    }

    @Override
    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    @Override
    public void setNumberOfRooms(int numberOfRooms) {
        if (numberOfRooms >= 0) {
            this.numberOfRooms = numberOfRooms;
        } else throw new InvalidRoomsCountException(numberOfRooms);
    }

    @Override
    public double getSquare() {
        return square;
    }

    @Override
    public void setSquare(double square) {
        if (square >= 0) {
            this.square = square;
        } else throw new InvalidSpaceAreaException(square);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Office (");
        sb.append(numberOfRooms);
        sb.append(", ");
        sb.append(square);
        sb.append(")");
        return sb.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof Office)) {
            return false;
        }
        Office office = (Office) object;
        return (this.numberOfRooms == office.numberOfRooms) && (this.square == office.square);
    }

    @Override
    public int hashCode() {
        Double d = new Double(square);
        int hash;
        long dlong = d.longValue();
        hash = numberOfRooms ^ ((int) (dlong >> 32)) ^ ((int) ((dlong << 32) >> 32));
        return hash;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
