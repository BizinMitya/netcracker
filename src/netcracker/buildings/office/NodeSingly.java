package netcracker.buildings.office;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import netcracker.Space;

/**
 * @author Дмитрий
 */
public class NodeSingly {
    public NodeSingly next;
    public Space office;

    public NodeSingly() {
        this.next = null;
        this.office = null;
    }

    public NodeSingly(Space office) {
        this.next = null;
        this.office = office;
    }

}
