package netcracker.buildings.office;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import netcracker.Space;
import netcracker.SpaceIndexOutOfBoundsException;

/**
 * @author bizin
 */
public class SinglyLinkedList implements Cloneable {
    private NodeSingly head;

    public SinglyLinkedList() {
        head = new NodeSingly();
        head.next = head;
    }

    public NodeSingly getNode(int number) {
        NodeSingly t = head;
        int i = -1;//номер головы
        while (i != number) {
            if (t.next != head) {
                i++;
                t = t.next;
            } else throw new SpaceIndexOutOfBoundsException("Выход за границы номеров офисов!\n", number);
        }
        return t;
    }

    public void addNode(int number, NodeSingly node)//по номеру узла и самому узлу
    {
        if (number == 0)//добавляем первый элемент в список
        {
            if (head.next == head)//в списке нет элементов
            {
                head.next = node;
                node.next = head;//замыкаем кольцо
            } else//есть элементы
            {
                node.next = head.next;
                head.next = node;
            }
        } else {
            NodeSingly t = head;
            int i = -1;
            while (i != number - 1) {
                if (t.next != head) {
                    i++;
                    t = t.next;
                } else
                    throw new SpaceIndexOutOfBoundsException("Выход за границы номеров офисов!\nОфис не добавлен!\n", number);
            }
            if (t.next == head)//добавление в конец
            {
                t.next = node;
                node.next = head;//замыкание
            } else//добавление в середину
            {
                node.next = t.next;
                t.next = node;
            }
        }
    }

    public void delNode(int number) {
        if (number == 0 && head.next == head)//список пуст
        {
            throw new SpaceIndexOutOfBoundsException("Выход за границы номеров офисов!\nСписок пуст!\n", number);
        } else if (number == 0 && head.next != head) {
            NodeSingly d = head.next;
            if (d.next == head)//в списке только один элемент
            {
                d.next = null;
                //d=null;
                head.next = head;//замыкание
            } else {
                head.next = d.next;
                d.next = null;
                //d=null;
            }
        } else if (number != 0) {
            int i = -1;
            NodeSingly t = head;
            NodeSingly d;
            while (i != number - 1) {
                if (t.next != head) {
                    i++;
                    t = t.next;
                } else
                    throw new SpaceIndexOutOfBoundsException("Выход за границы номеров офисов!\nОфис не удалён!\n", number);
            }
            if (t.next == head) {
                throw new SpaceIndexOutOfBoundsException("Выход за границы номеров офисов!\nОфис не удалён!\n", number);
            } else {
                d = t.next;
                if (d.next == head)//удаление последнего в списке
                {
                    d.next = null;
                    //d=null;
                    t.next = head;//замыкаем
                } else//удаление из середины
                {
                    t.next = d.next;
                    d.next = null;
                    //d=null;
                }
            }
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        SinglyLinkedList list = (SinglyLinkedList) super.clone();
        NodeSingly head = new NodeSingly();
        list.head = head;
        NodeSingly t = this.head;
        NodeSingly tmp = list.head;
        while (t.next != this.head) {
            t = t.next;
            NodeSingly temp = new NodeSingly((Space) t.office.clone());
            tmp.next = temp;
            tmp = tmp.next;
            tmp.next = list.head;
        }
        return list;
    }
}
