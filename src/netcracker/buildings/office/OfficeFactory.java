package netcracker.buildings.office;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import netcracker.BuildingFactory;
import netcracker.Floor;
import netcracker.Space;

/**
 * @author Дмитрий
 */
public class OfficeFactory implements BuildingFactory {

    @Override
    public Office createSpace(double area) {
        return new Office(area);
    }

    @Override
    public Office createSpace(int roomsCount, double area) {
        return new Office(area, roomsCount);
    }

    @Override
    public OfficeFloor createFloor(int spacesCount) {
        return new OfficeFloor(spacesCount);
    }

    @Override
    public OfficeFloor createFloor(Space[] spaces) {
        return new OfficeFloor(spaces);
    }

    @Override
    public OfficeBuilding createBuilding(int floorsCount, int[] spacesCounts) {
        return new OfficeBuilding(floorsCount, spacesCounts);
    }

    @Override
    public OfficeBuilding createBuilding(Floor[] floors) {
        return new OfficeBuilding(floors);
    }

}
