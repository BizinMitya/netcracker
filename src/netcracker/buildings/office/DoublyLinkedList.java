package netcracker.buildings.office;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import netcracker.Floor;
import netcracker.FloorIndexOutOfBoundsException;

/**
 * @author Дмитрий
 */
public class DoublyLinkedList implements Cloneable {
    private NodeDoubly head;

    public DoublyLinkedList() {
        head = new NodeDoubly();
        head.next = head;
        head.prev = head;
    }

    public NodeDoubly getNode(int number) {
        NodeDoubly t = head;
        int i = -1;//номер головы
        while (i != number) {
            if (t.next != head) {
                i++;
                t = t.next;
            } else throw new FloorIndexOutOfBoundsException("Выход за границы номеров этажей!\n", number);
        }
        return t;
    }

    public void addNode(int number, NodeDoubly node) {
        if (number == 0)//добавляем первый элемент в список
        {
            if (head.next == head)//в списке нет элементов
            {
                head.next = node;
                node.prev = head;
                node.next = head;//замыкаем кольцо
                head.prev = node;
            } else//есть элементы
            {
                node.next = head.next;
                node.next.prev = node;
                head.next = node;
                node.prev = head;
            }
        } else {
            NodeDoubly t = head;
            int i = -1;
            while (i != number - 1) {
                if (t.next != head) {
                    i++;
                    t = t.next;
                } else
                    throw new FloorIndexOutOfBoundsException("Выход за границы номеров этажей!Узел не добавлен!\n", number);
            }
            if (t.next == head)//добавление в конец
            {
                t.next = node;
                node.prev = t;
                node.next = head;//замыкание
                head.prev = node;
            } else//добавление в середину
            {
                node.next = t.next;
                node.next.prev = node;
                t.next = node;
                node.prev = t;
            }
        }
    }

    public void delNode(int number) {
        if (number == 0 && head.next == head)//список пуст
        {
            throw new FloorIndexOutOfBoundsException("Список пуст!Узел не удалён!\n", number);
        } else if (number == 0 && head.next != head) {
            NodeDoubly d = head.next;
            if (d.next == head)//в списке только один элемент
            {
                d.next = null;
                d.prev = null;
                //d=null;
                head.next = head;//замыкание
                head.prev = head;
            } else {
                head.next = d.next;
                d.next = null;
                d.prev = null;
                //d=null;
                head.next.prev = head;
            }
        } else if (number != 0) {
            int i = -1;
            NodeDoubly t = head;
            NodeDoubly d;
            while (i != number - 1) {
                if (t.next != head) {
                    i++;
                    t = t.next;
                } else
                    throw new FloorIndexOutOfBoundsException("Выход за границы номеров этажей!Узел не удалён!\n", number);//в списке: head->0->1->2->3->head,а хотят удалить >4
            }
            if (t.next == head)//в списке: head->0->1->2->3->head,а хотят удалить 4
            {
                throw new FloorIndexOutOfBoundsException("Выход за границы номеров этажей!Узел не удалён!\n", number);
            } else {
                d = t.next;
                if (d.next == head)//удаление последнего в списке
                {
                    d.next = null;
                    d.prev = null;
                    //d=null;
                    t.next = head;//замыкаем
                    head.prev = t;
                } else//удаление из середины
                {
                    t.next = d.next;
                    d.next = null;
                    d.prev = null;
                    //d=null;
                    t.next.prev = t;
                }
            }
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DoublyLinkedList list = (DoublyLinkedList) super.clone();
        NodeDoubly head = new NodeDoubly();
        list.head = head;
        NodeDoubly t = this.head;
        NodeDoubly tmp = list.head;
        while (t.next != this.head) {
            t = t.next;
            NodeDoubly temp = new NodeDoubly((Floor) t.floor.clone());
            tmp.next = temp;
            tmp = tmp.next;
            tmp.next = list.head;
        }
        return list;
    }
}
