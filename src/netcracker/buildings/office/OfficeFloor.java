package netcracker.buildings.office;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import netcracker.Floor;
import netcracker.Space;
import netcracker.SpaceIndexOutOfBoundsException;
import netcracker.SpaceIterator;

import java.io.Serializable;
import java.util.Iterator;

/**
 * @author bizin
 */
public class OfficeFloor implements Floor, Serializable, Cloneable {
    private SinglyLinkedList offices;

    public OfficeFloor(int numberOfOffice)//нумерация с 0
    {
        if (numberOfOffice > 0) {
            this.offices = new SinglyLinkedList();
            for (int i = 0; i < numberOfOffice; i++) {
                Space office = new Office();//офис по умолчанию
                NodeSingly t = new NodeSingly(office);//узел по умолчанию
                this.offices.addNode(i, t);//список из офисов по умолчанию
            }
        } else throw new SpaceIndexOutOfBoundsException("Неправильное количество офисов!\n", numberOfOffice);
    }

    public OfficeFloor(Space[] offices) {
        if (offices.length > 0) {
            this.offices = new SinglyLinkedList();
            NodeSingly t;
            for (int i = 0; i < offices.length; i++) {
                t = new NodeSingly(offices[i]);//узел - аналог ячейки массива
                this.offices.addNode(i, t);
            }
        } else throw new SpaceIndexOutOfBoundsException("Передан массив нулевой длины!\n");
    }

    @Override
    public int numberOfSpaces() {
        int i = 0;
        NodeSingly head = offices.getNode(-1);
        NodeSingly t = head;
        while (t.next != head) {
            i++;
            t = t.next;
        }
        return i;
    }

    @Override
    public double totalSquare() {
        NodeSingly head = offices.getNode(-1);
        NodeSingly t = head;
        double sum = 0.0;
        if (t.next == head) {
            throw new SpaceIndexOutOfBoundsException("Выход за границы номеров офисов!\n");
        } else {
            t = t.next;
            while (t != head) {
                sum += t.office.getSquare();
                t = t.next;
            }
            return sum;
        }
    }

    @Override
    public int totalRooms() {
        NodeSingly head = offices.getNode(-1);
        NodeSingly t = head;
        int sum = 0;
        if (t.next == head) {
            throw new SpaceIndexOutOfBoundsException("Выход за границы номеров офисов!\nСписок пуст!\n");
        } else {
            t = t.next;
            while (t != head) {
                sum += t.office.getNumberOfRooms();
                t = t.next;
            }
            return sum;
        }
    }

    @Override
    public Space[] arrayOfSpaces() {
        int k = this.numberOfSpaces();
        Space[] array = new Space[k];
        if (k == 0) {
            throw new SpaceIndexOutOfBoundsException("Выход за границы номеров офисов!\nСписок пуст!\n");
        } else {
            NodeSingly head = offices.getNode(-1);
            NodeSingly t = head.next;
            int i = 0;
            while (t != head) {
                array[i] = t.office;
                i++;
                t = t.next;
            }
            return array;
        }
    }

    @Override
    public Space getSpace(int numberOfOffice) {
        return offices.getNode(numberOfOffice).office;
    }

    @Override
    public void changeSpace(int numberOfOffice, Space office) {
        offices.getNode(numberOfOffice).office = office;
    }

    @Override
    public void addSpace(int numberOfOffice, Space office) {
        NodeSingly node = new NodeSingly(office);
        offices.addNode(numberOfOffice, node);
    }

    @Override
    public void deleteSpace(int numberOfOffice) {
        offices.delNode(numberOfOffice);
    }

    @Override
    public Space getBestSpace() {
        NodeSingly head = offices.getNode(-1);
        NodeSingly t = head;
        if (t.next == head) {
            throw new SpaceIndexOutOfBoundsException("Выход за границы номеров офисов!\nСписок пуст!\n");
        } else {
            t = t.next;
            double max = t.office.getSquare();
            Space maxOffice = offices.getNode(0).office;
            maxOffice = t.office;
            while (t != head) {
                if (t.office.getSquare() >= max) {
                    max = t.office.getSquare();
                    maxOffice = t.office;
                }
                t = t.next;
            }
            return maxOffice;
        }
    }

    @Override
    public Iterator iterator() {
        return new SpaceIterator(this);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("OfficeFloor (");
        sb.append(this.numberOfSpaces());
        for (int i = 0; i < this.numberOfSpaces(); i++) {
            sb.append(", ");
            sb.append(this.getSpace(i).toString());
        }
        sb.append(')');
        return sb.toString();
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (!(object instanceof OfficeFloor)) return false;
        OfficeFloor of = (OfficeFloor) object;
        if (of.numberOfSpaces() != this.numberOfSpaces()) return false;
        else {
            boolean t = true;
            Space[] array1 = this.arrayOfSpaces();
            Space[] array2 = of.arrayOfSpaces();
            for (int i = 0; i < this.numberOfSpaces(); i++) {
                if (!array1[i].equals(array2[i])) t = false;
            }
            return t;
        }
    }

    @Override
    public int hashCode() {
        int hash = this.numberOfSpaces();
        for (int i = 0; i < this.numberOfSpaces(); i++) {
            hash = hash ^ this.getSpace(i).hashCode();
        }
        return hash;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        OfficeFloor of = (OfficeFloor) super.clone();
        of.offices = (SinglyLinkedList) offices.clone();
        return of;
    }
} 
