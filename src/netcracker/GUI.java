package netcracker;

import netcracker.buildings.dwelling.Dwelling;
import netcracker.buildings.dwelling.DwellingFactory;
import netcracker.buildings.office.OfficeBuilding;
import netcracker.buildings.office.OfficeFactory;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Дмитрий on 23.11.2015.
 */
public class GUI extends JFrame {
    public void createGUI() {
        JFrame frame = new JFrame("Информационная система о зданиях");
        frame.setMaximumSize(new Dimension(501, 501));
        frame.setMinimumSize(new Dimension(501, 501));
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenu lookAndFeelMenu = new JMenu("Look&Feel");
        JRadioButtonMenuItem radioButtonMenuItem1 = new JRadioButtonMenuItem("Metal");
        JRadioButtonMenuItem radioButtonMenuItem2 = new JRadioButtonMenuItem("Nimbus");
        JRadioButtonMenuItem radioButtonMenuItem3 = new JRadioButtonMenuItem("CDE/Motif");
        JRadioButtonMenuItem radioButtonMenuItem4 = new JRadioButtonMenuItem("Windows");
        JRadioButtonMenuItem radioButtonMenuItem5 = new JRadioButtonMenuItem("Windows Classic");
        lookAndFeelMenu.add(radioButtonMenuItem1);
        lookAndFeelMenu.add(radioButtonMenuItem2);
        lookAndFeelMenu.add(radioButtonMenuItem3);
        lookAndFeelMenu.add(radioButtonMenuItem4);
        lookAndFeelMenu.add(radioButtonMenuItem5);
        JMenuItem openDwellingMenu = new JMenuItem("Open dwelling…");
        fileMenu.add(openDwellingMenu);
        JMenuItem openOfficeBuildingMenu = new JMenuItem("Open office building…");
        fileMenu.add(openOfficeBuildingMenu);
        menuBar.add(fileMenu);
        menuBar.add(lookAndFeelMenu);

        frame.setJMenuBar(menuBar);
        frame.setPreferredSize(new Dimension(270, 225));

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        Border solidBorder = BorderFactory.createLineBorder(Color.BLACK, 1);

        JLabel infoOfBuilding = new JLabel();
        infoOfBuilding.setVerticalAlignment(JLabel.CENTER);
        infoOfBuilding.setHorizontalAlignment(JLabel.CENTER);
        infoOfBuilding.setBorder(solidBorder);
        infoOfBuilding.setForeground(Color.BLACK);

        JLabel infoOfFloor = new JLabel();
        infoOfFloor.setVerticalAlignment(JLabel.CENTER);
        infoOfFloor.setHorizontalAlignment(JLabel.CENTER);
        infoOfFloor.setBorder(solidBorder);
        infoOfFloor.setForeground(Color.BLACK);

        JLabel infoOfSpace = new JLabel();
        infoOfSpace.setVerticalAlignment(JLabel.CENTER);
        infoOfSpace.setHorizontalAlignment(JLabel.CENTER);
        infoOfSpace.setBorder(solidBorder);
        infoOfSpace.setForeground(Color.BLACK);

        JLabel plan = new JLabel();
        plan.setBorder(solidBorder);
        plan.setPreferredSize(new Dimension(frame.getWidth(), frame.getHeight() / 2));

        JScrollPane scrollPane = new JScrollPane(plan, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        panel.add(infoOfBuilding, BorderLayout.LINE_START);
        panel.add(infoOfFloor, BorderLayout.CENTER);
        panel.add(infoOfSpace, BorderLayout.LINE_END);
        panel.add(plan, BorderLayout.PAGE_END);

        frame.getContentPane().add(panel);
        infoOfBuilding.setPreferredSize(new Dimension(frame.getWidth() / 3, frame.getHeight() / 2));
        infoOfFloor.setPreferredSize(new Dimension(frame.getWidth() / 3, frame.getHeight() / 2));
        infoOfSpace.setPreferredSize(new Dimension(frame.getWidth() / 3, frame.getHeight() / 2));

        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);


        openDwellingMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileOpen = new JFileChooser();
                fileOpen.setCurrentDirectory(new File("C://Users//Дмитрий//Desktop//NetCracker"));
                int ret = fileOpen.showDialog(null, "Открыть файл");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileOpen.getSelectedFile();
                    try {
                        Buildings.setBuildingFactory(new DwellingFactory());
                        Dwelling dwelling = (Dwelling) Buildings.readBuilding(new FileReader(file));
                        infoOfBuilding.setText("<html>Тип здания: Dwelling<br>" + "Количество этажей: " + dwelling.getNumberOfFloors()
                                + "<br>" + "Общая площадь: " + dwelling.getSquareOfRooms() + "<br></html>");
                        plan.setLayout(new BoxLayout(plan, BoxLayout.Y_AXIS));
                        JPanel[] panels = new JPanel[dwelling.getNumberOfFloors()];
                        for (int i = 0; i < dwelling.getNumberOfFloors(); i++) {
                            JPanel floorPanel = new JPanel();
                            panels[i] = floorPanel;
                            floorPanel.setLayout(new BorderLayout());
                            floorPanel.setBorder(solidBorder);
                            floorPanel.setPreferredSize(new Dimension(frame.getWidth(), frame.getHeight() / 2 * dwelling.getNumberOfFloors()));
                            plan.add(floorPanel);
                        }
                        for (int i = 0; i < dwelling.getNumberOfFloors(); i++) {
                            panels[i].setLayout(new FlowLayout(FlowLayout.LEFT));
                            for (int j = 0; j < dwelling.getFloor(i).numberOfSpaces(); j++) {
                                JButton space = new JButton();
                                panels[i].add(space);
                            }
                        }
                    } catch (IOException | NegativeArraySizeException | FloorIndexOutOfBoundsException e1) {
                        JOptionPane.showOptionDialog(null, "Ошибка чтения файла!", "Ошибка", JOptionPane.OK_OPTION,
                                JOptionPane.INFORMATION_MESSAGE, null, new Object[]{"ОК"}, null);
                    }
                }
            }
        });

        openOfficeBuildingMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileOpen = new JFileChooser();
                fileOpen.setCurrentDirectory(new File("C://Users//Дмитрий//Desktop//NetCracker"));
                int ret = fileOpen.showDialog(null, "Открыть файл");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileOpen.getSelectedFile();
                    try {
                        Buildings.setBuildingFactory(new OfficeFactory());
                        OfficeBuilding officeBuilding = (OfficeBuilding) Buildings.readBuilding(new FileReader(file));
                        infoOfBuilding.setText("<html>Тип здания: OfficeBuilding<br>" + "Количество этажей: " + officeBuilding.getNumberOfFloors()
                                + "<br>" + "Общая площадь: " + officeBuilding.getSquareOfRooms() + "<br></html>");
                        plan.setLayout(new BoxLayout(plan, BoxLayout.Y_AXIS));
                        JPanel[] panels = new JPanel[officeBuilding.getNumberOfFloors()];
                        for (int i = 0; i < officeBuilding.getNumberOfFloors(); i++) {
                            JPanel floorPanel = new JPanel();
                            panels[i] = floorPanel;
                            floorPanel.setLayout(new BorderLayout());
                            floorPanel.setBorder(solidBorder);
                            floorPanel.setPreferredSize(new Dimension(frame.getWidth(), frame.getHeight() / 2 * officeBuilding.getNumberOfFloors()));
                            plan.add(floorPanel);
                        }
                        for (int i = 0; i < officeBuilding.getNumberOfFloors(); i++) {
                            panels[i].setLayout(new FlowLayout(FlowLayout.LEFT));
                            for (int j = 0; j < officeBuilding.getFloor(i).numberOfSpaces(); j++) {
                                JButton space = new JButton();
                                panels[i].add(space);
                            }
                        }

                    } catch (IOException | NegativeArraySizeException | FloorIndexOutOfBoundsException e1) {
                        JOptionPane.showOptionDialog(null, "Ошибка чтения файла!", "Ошибка", JOptionPane.OK_OPTION,
                                JOptionPane.INFORMATION_MESSAGE, null, new Object[]{"ОК"}, null);
                    }
                }
            }
        });

        radioButtonMenuItem1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                radioButtonMenuItem2.setSelected(false);
                radioButtonMenuItem3.setSelected(false);
                radioButtonMenuItem4.setSelected(false);
                radioButtonMenuItem5.setSelected(false);
                try {
                    UIManager.setLookAndFeel(UIManager.getInstalledLookAndFeels()[0].getClassName());
                    SwingUtilities.updateComponentTreeUI(frame);
                } catch (ClassNotFoundException | UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException e1) {
                    e1.printStackTrace();
                }
            }
        });

        radioButtonMenuItem2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                radioButtonMenuItem1.setSelected(false);
                radioButtonMenuItem3.setSelected(false);
                radioButtonMenuItem4.setSelected(false);
                radioButtonMenuItem5.setSelected(false);
                try {
                    UIManager.setLookAndFeel(UIManager.getInstalledLookAndFeels()[1].getClassName());
                    SwingUtilities.updateComponentTreeUI(frame);
                } catch (ClassNotFoundException | UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException e1) {
                    e1.printStackTrace();
                }
            }
        });

        radioButtonMenuItem3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                radioButtonMenuItem1.setSelected(false);
                radioButtonMenuItem2.setSelected(false);
                radioButtonMenuItem4.setSelected(false);
                radioButtonMenuItem5.setSelected(false);
                try {
                    UIManager.setLookAndFeel(UIManager.getInstalledLookAndFeels()[2].getClassName());
                    SwingUtilities.updateComponentTreeUI(frame);
                } catch (ClassNotFoundException | UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException e1) {
                    e1.printStackTrace();
                }
            }
        });

        radioButtonMenuItem4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                radioButtonMenuItem1.setSelected(false);
                radioButtonMenuItem2.setSelected(false);
                radioButtonMenuItem3.setSelected(false);
                radioButtonMenuItem5.setSelected(false);
                try {
                    UIManager.setLookAndFeel(UIManager.getInstalledLookAndFeels()[3].getClassName());
                    SwingUtilities.updateComponentTreeUI(frame);
                } catch (ClassNotFoundException | UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException e1) {
                    e1.printStackTrace();
                }
            }
        });

        radioButtonMenuItem5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                radioButtonMenuItem1.setSelected(false);
                radioButtonMenuItem2.setSelected(false);
                radioButtonMenuItem3.setSelected(false);
                radioButtonMenuItem4.setSelected(false);
                try {
                    UIManager.setLookAndFeel(UIManager.getInstalledLookAndFeels()[4].getClassName());
                    SwingUtilities.updateComponentTreeUI(frame);
                } catch (ClassNotFoundException | UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }
}