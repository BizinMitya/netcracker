package netcracker;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Дмитрий
 */
public interface Floor {

    void addSpace(int numberOfSpace, Space space);

    Space[] arrayOfSpaces();

    void changeSpace(int numberOfSpace, Space space);

    void deleteSpace(int numberOfSpace);

    Space getBestSpace();

    Space getSpace(int numberOfSpace);

    int numberOfSpaces();

    int totalRooms();

    double totalSquare();

    java.util.Iterator iterator();

    Object clone() throws CloneNotSupportedException;

}
