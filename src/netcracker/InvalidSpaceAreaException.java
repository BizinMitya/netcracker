package netcracker;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * @author Дмитрий
 */
public class InvalidSpaceAreaException extends IllegalArgumentException {
    private String message;
    private double square;

    public InvalidSpaceAreaException() {
        message = "Неправильно задана площадь помещения!";
    }

    public InvalidSpaceAreaException(double square) {
        message = "Неправильно задана площадь помещения:" + square + "!";
        this.square = square;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
