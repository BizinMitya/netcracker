package netcracker;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * @author Дмитрий
 */
public class InexchangeableSpacesException extends Exception {
    private String message;

    public InexchangeableSpacesException() {
        message = "Несоответствие обменивающихся помещений!";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
