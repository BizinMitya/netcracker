package netcracker;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Дмитрий
 */
public class InexchangeableFloorsException extends Exception {
    private String message;

    public InexchangeableFloorsException() {
        message = "Несоответствие обменивающихся этажей!";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
